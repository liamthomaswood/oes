﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OES.Entities.Answers;
using OES.Entities.Questions;
using OES.Entities.QuestionAnswers;
using OES.Entities.Emails;
using OES.Entities.EmailTrackers;
using OES.Entities.Employees;


namespace OES.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("ApplicationDbcontext") { }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionAnswer> QuestionAnswers { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<EmailTracker> EmailTrackers { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
