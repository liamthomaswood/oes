namespace OES.Data.Migrations
{
    using OES.Entities.Employees;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OES.Data.Context.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OES.Data.Context.ApplicationDbContext context)
        {
            var employees = new List<Employee>
            {
                new Employee
                {
                    EmployeeId = Guid.NewGuid(),
                    FirstName = "Liam",
                    LastName = "wood",
                    Role = Roles.Developer
                },
                new Employee
                {
                    EmployeeId = Guid.NewGuid(),
                    FirstName = "Andy",
                    LastName = "Crouch",
                    Role = Roles.Manager
                }
            };
            employees.ForEach(e => context.Employees.Add(e));
            context.SaveChanges();
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
