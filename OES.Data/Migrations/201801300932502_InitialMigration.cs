namespace OES.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        AnswerId = c.Guid(nullable: false),
                        EmployeeId = c.Guid(nullable: false),
                        Value = c.String(),
                        QuestionAnswer_Id = c.Guid(),
                        QuestionAnswer_QuestionId = c.Guid(),
                        QuestionAnswer_AnswerId = c.Guid(),
                    })
                .PrimaryKey(t => new { t.AnswerId, t.EmployeeId })
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.QuestionAnswers", t => new { t.QuestionAnswer_Id, t.QuestionAnswer_QuestionId, t.QuestionAnswer_AnswerId })
                .Index(t => t.EmployeeId)
                .Index(t => new { t.QuestionAnswer_Id, t.QuestionAnswer_QuestionId, t.QuestionAnswer_AnswerId });
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Guid(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 128),
                        LastName = c.String(nullable: false, maxLength: 128),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        EmailId = c.Guid(nullable: false),
                        EmployeeId = c.Guid(nullable: false),
                        Questions = c.String(),
                        EmailTracker_EmailTrackerId = c.Guid(),
                        EmailTracker_EmailId = c.Guid(),
                    })
                .PrimaryKey(t => new { t.EmailId, t.EmployeeId })
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.EmailTrackers", t => new { t.EmailTracker_EmailTrackerId, t.EmailTracker_EmailId })
                .Index(t => t.EmployeeId)
                .Index(t => new { t.EmailTracker_EmailTrackerId, t.EmailTracker_EmailId });
            
            CreateTable(
                "dbo.EmailTrackers",
                c => new
                    {
                        EmailTrackerId = c.Guid(nullable: false),
                        EmailId = c.Guid(nullable: false),
                        TimeSent = c.DateTime(nullable: false),
                        TimeReplied = c.DateTime(nullable: false),
                        ReminderSent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.EmailTrackerId, t.EmailId });
            
            CreateTable(
                "dbo.QuestionAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuestionId = c.Guid(nullable: false),
                        AnswerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Id, t.QuestionId, t.AnswerId });
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        QuestionId = c.Guid(nullable: false),
                        Text = c.String(nullable: false),
                        QuestionCategory = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionId);
            
            CreateTable(
                "dbo.QuestionAnswerQuestions",
                c => new
                    {
                        QuestionAnswer_Id = c.Guid(nullable: false),
                        QuestionAnswer_QuestionId = c.Guid(nullable: false),
                        QuestionAnswer_AnswerId = c.Guid(nullable: false),
                        Question_QuestionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuestionAnswer_Id, t.QuestionAnswer_QuestionId, t.QuestionAnswer_AnswerId, t.Question_QuestionId })
                .ForeignKey("dbo.QuestionAnswers", t => new { t.QuestionAnswer_Id, t.QuestionAnswer_QuestionId, t.QuestionAnswer_AnswerId }, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_QuestionId, cascadeDelete: true)
                .Index(t => new { t.QuestionAnswer_Id, t.QuestionAnswer_QuestionId, t.QuestionAnswer_AnswerId })
                .Index(t => t.Question_QuestionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuestionAnswerQuestions", "Question_QuestionId", "dbo.Questions");
            DropForeignKey("dbo.QuestionAnswerQuestions", new[] { "QuestionAnswer_Id", "QuestionAnswer_QuestionId", "QuestionAnswer_AnswerId" }, "dbo.QuestionAnswers");
            DropForeignKey("dbo.Answers", new[] { "QuestionAnswer_Id", "QuestionAnswer_QuestionId", "QuestionAnswer_AnswerId" }, "dbo.QuestionAnswers");
            DropForeignKey("dbo.Emails", new[] { "EmailTracker_EmailTrackerId", "EmailTracker_EmailId" }, "dbo.EmailTrackers");
            DropForeignKey("dbo.Answers", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Emails", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.QuestionAnswerQuestions", new[] { "Question_QuestionId" });
            DropIndex("dbo.QuestionAnswerQuestions", new[] { "QuestionAnswer_Id", "QuestionAnswer_QuestionId", "QuestionAnswer_AnswerId" });
            DropIndex("dbo.Emails", new[] { "EmailTracker_EmailTrackerId", "EmailTracker_EmailId" });
            DropIndex("dbo.Emails", new[] { "EmployeeId" });
            DropIndex("dbo.Answers", new[] { "QuestionAnswer_Id", "QuestionAnswer_QuestionId", "QuestionAnswer_AnswerId" });
            DropIndex("dbo.Answers", new[] { "EmployeeId" });
            DropTable("dbo.QuestionAnswerQuestions");
            DropTable("dbo.Questions");
            DropTable("dbo.QuestionAnswers");
            DropTable("dbo.EmailTrackers");
            DropTable("dbo.Emails");
            DropTable("dbo.Employees");
            DropTable("dbo.Answers");
        }
    }
}
