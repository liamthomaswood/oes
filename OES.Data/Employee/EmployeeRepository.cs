﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OES.Entities.Employee;
using OES.Entities.Employee.ViewModels;
using OES.Data.Context;
using System.Data.Entity;

namespace OES.Data.Employee
{
    public class EmployeeRepository
    {
        public List<EmployeeDisplayViewModel> GetEmployees()
        {
            using (var context = new ApplicationDbContext())
            {
                List<Entities.Employees.Employee> employees = new List<Entities.Employees.Employee>();
                employees = context.Employees.AsNoTracking()
                    .Include(x => x.Email)
                    .ToList();

                if (employees != null)
                {
                    List<EmployeeDisplayViewModel> employeesDisplay = new List<EmployeeDisplayViewModel>();
                    foreach (var x in employees)
                    { var employeeDisplay = new EmployeeDisplayViewModel()
                        {
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            Role = x.Role.ToString()
                        };
                    employeesDisplay.Add(employeeDisplay);
                    }
                    return employeesDisplay;
                }
            return null;
            }            
        }
    }
}
