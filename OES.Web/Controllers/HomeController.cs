﻿using OES.Data.Employee;
using System.Web.Mvc;

namespace OES.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var repo = new EmployeeRepository();
            var employeesList = repo.GetEmployees();
            return View(employeesList);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}