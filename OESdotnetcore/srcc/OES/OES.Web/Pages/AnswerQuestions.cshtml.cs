﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OES.Core.Models;
using OES.Core.Services.Interfaces;

namespace OES.Web.Pages
{
    public class AnswerQuestionsModel : PageModel
    {
        private readonly IAnswerService _answerService;
        private readonly IQuestionService _questionService;

        public AnswerQuestionsModel(
            IAnswerService answerService,
            IQuestionService questionService)
        {
            _answerService = answerService;
            _questionService = questionService;
        }

        [BindProperty]
        public Answer Answer { get; set; }

        [BindProperty]
        public List<int> AvailableChoices { get; set; }

        public List<Question> EmployeeTextQuestions { get; set; }
        public List<Question> EmployeeChoiceQuestions { get; set; }
        public List<Question> EmployeeFrequencyQuestions { get; set; }
        public string ReturnUrl { get; set; }

        public void OnGet(string returnUrl = null)
        {
            var employeeId = Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

            var employeeQuestions = _questionService.GetUnansweredQuestionsForEmployee(employeeId).ToList();

            var textQuestions = employeeQuestions
                .Where(tq => tq.ResponseCategory == QuestionCategory.FreeText)
                .ToList();

            var choiceQuestions = employeeQuestions
                .Where(tq => tq.ResponseCategory == QuestionCategory.Choice)
                .ToList();

            var frequencyQuestions = employeeQuestions
                .Where(tq => tq.ResponseCategory == QuestionCategory.Frequency)
                .ToList();

            EmployeeTextQuestions = textQuestions;
            EmployeeChoiceQuestions = choiceQuestions;
            EmployeeFrequencyQuestions = frequencyQuestions;

            AvailableChoices = Enumerable.Range(1, 5).ToList();
        }

        public IActionResult OnPost(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                var answer = Answer;
                _answerService.AnswerQuestion(answer, Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));

                return RedirectToPage("/AnswerQuestions");
            }

            return Page();
        }
    }
}