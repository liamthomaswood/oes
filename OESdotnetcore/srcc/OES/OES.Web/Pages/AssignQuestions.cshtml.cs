﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using OES.Core.Models;
using OES.Core.Services.Interfaces;

namespace OES.Web.Pages
{
    public class AssignQuestionsModel : PageModel
    {
        private readonly IQuestionAssignmentService _questionAssignmentService;
        private readonly IQuestionService _questionService;
        private readonly IEmployeeService _employeeService;

        [BindProperty]
        public List<EmployeeQuestion> EmployeeQuestionList { get; set; }

        [BindProperty]
        public List<Guid> QuestionIds { get; set; }

        [BindProperty]
        public List<Guid> EmployeeIds { get; set; }

        public SelectList EmployeeSelectList { get; set; }
        public SelectList QuestionsSelectList { get; set; }

        public AssignQuestionsModel(
            IQuestionAssignmentService questionAssignmentService,
            IQuestionService questionService,
            IEmployeeService employeeService)
        {
            _questionAssignmentService = questionAssignmentService;
            _questionService = questionService;
            _employeeService = employeeService;
        }

        public void OnGet()
        {
            EmployeeSelectList = new SelectList(_employeeService.FetchAllEmployees(), nameof(Employee.EmployeeId), nameof(Employee.EmailAddress));
            QuestionsSelectList = new SelectList(_questionService.GetAllQuestions(), nameof(Question.QuestionId), nameof(Question.Text));

        }

        public IActionResult OnPost()
        {

            if (ModelState.IsValid)
            {
                _questionAssignmentService.AssignQuestionsToEmployeesMultiple(QuestionIds, EmployeeIds);
                return RedirectToPage("/Assignquestions");
            }

            return Page();
        }
    }
}