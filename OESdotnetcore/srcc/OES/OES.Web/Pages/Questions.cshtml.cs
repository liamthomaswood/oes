﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using OES.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Linq;

namespace OES.Web.Pages
{
    public class QuestionsModel : PageModel
    {
        private readonly IQuestionService _questionService;
        private readonly IEmployeeService _employeeService;
        private readonly UserManager<OESWebUser> _userManager;

        public QuestionsModel(
            IQuestionService questionService,
            IEmployeeService employeeService,
            UserManager<OESWebUser> userManager)
        {
            _questionService = questionService;
            _employeeService = employeeService;
            _userManager = userManager;
        }

        [BindProperty]
        public Question Question { get; set; }

        [BindProperty]
        public QuestionCategory ChosenCategory { get; set; }

        public List<QuestionCategory> QuestionCategories { get; set; }
        public List<Question> Questions { get; set; }
        public Employee Employee { get; set; }

        public void OnGet()
        {            
            Employee = _employeeService.GetEmployeeByEmail(User.Identity.Name);
            Questions = _questionService.GetAllQuestions().ToList();
            Question = new Question() { QuestionId = Guid.NewGuid() };
            QuestionCategories = new List<QuestionCategory>
            {
                QuestionCategory.FreeText,
                QuestionCategory.Choice,
                QuestionCategory.Frequency
            };

            var employeeIsBaseUser = Employee.Role == Core.Enumerations.Roles.BaseUser;

            if (employeeIsBaseUser)
            {
                var userId = Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                ShowBaseUserView(userId);
            }
        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var question = Question;
                question.ResponseCategory = ChosenCategory;
                
                _questionService.AddQuestion(question);
                return RedirectToPage("/Questions");
            }
            return Page();
        }

        public void ShowBaseUserView(Guid id)
        {
            Questions = _questionService.GetQuestionsForEmployee(id).ToList();
        }
    }
}