﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OES.Data.Entities;
using OES.Core.Services.Interfaces;
using OES.Core.Models;

namespace OES.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IEmployeeService _employeeService;

        public IndexModel(
            IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public Employee Employee { get; set; }

        public void OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                var employee = _employeeService.GetEmployeeByEmail(User.Identity.Name);
                Employee = employee;
            }
        }        
    }
}
