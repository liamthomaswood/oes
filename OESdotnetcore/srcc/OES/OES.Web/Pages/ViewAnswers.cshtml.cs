﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using OES.Core.Services.Interfaces;
using OES.Core.ViewModels;
using System.Collections.Generic;

namespace OES.Web.Pages
{
    public class ViewAnswersModel : PageModel
    {
        private readonly IEmployeeQuestionAnswerService _employeeQuestionAnswerService;

        public List<EmployeeQuestionAnswer> EmployeeQuestionAnswers { get; set; }

        public ViewAnswersModel(IEmployeeQuestionAnswerService employeeQuestionAnswerService)
        {
            _employeeQuestionAnswerService = employeeQuestionAnswerService;
        }

        public void OnGet()
        {
            EmployeeQuestionAnswers = _employeeQuestionAnswerService.GetEmployeeQuestionAnswers();
        }
    }
}