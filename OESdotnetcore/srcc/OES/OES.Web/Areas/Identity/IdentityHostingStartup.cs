﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OES.Data;
using OES.Data.Entities;
using OES.Data.Infrastructure;

[assembly: HostingStartup(typeof(OES.Web.Areas.Identity.IdentityHostingStartup))]
namespace OES.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<OESContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("OESContextConnection")));

                services.AddDefaultIdentity<OESWebUser>()
                    .AddEntityFrameworkStores<OESContext>();
            });
        }
    }
}