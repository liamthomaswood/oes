﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OES.Data.Migrations
{
    public partial class columnReorder3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeId_EmployeeQuestionId_QuestionId",
                table: "EmployeeQuestions");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeQuestionId",
                table: "EmployeeQuestions",
                column: "EmployeeQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeQuestions_EmployeeId",
                table: "EmployeeQuestions",
                column: "EmployeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeQuestionId",
                table: "EmployeeQuestions");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeQuestions_EmployeeId",
                table: "EmployeeQuestions");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeId_EmployeeQuestionId_QuestionId",
                table: "EmployeeQuestions",
                columns: new[] { "EmployeeId", "EmployeeQuestionId", "QuestionId" });
        }
    }
}
