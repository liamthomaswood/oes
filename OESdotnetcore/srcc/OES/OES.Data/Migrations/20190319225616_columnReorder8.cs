﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OES.Data.Migrations
{
    public partial class columnReorder8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeQuestionId",
                table: "EmployeeQuestions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployeeQuestions",
                table: "EmployeeQuestions");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployeeQuestions",
                table: "EmployeeQuestions",
                column: "EmployeeQuestionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployeeQuestions",
                table: "EmployeeQuestions");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeQuestionId",
                table: "EmployeeQuestions",
                column: "EmployeeQuestionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployeeQuestions",
                table: "EmployeeQuestions",
                columns: new[] { "QuestionId", "EmployeeId" });
        }
    }
}
