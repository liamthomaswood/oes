﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OES.Data.Migrations
{
    public partial class answeredbooleq : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Answered",
                table: "EmployeeQuestions",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Answered",
                table: "EmployeeQuestions");
        }
    }
}
