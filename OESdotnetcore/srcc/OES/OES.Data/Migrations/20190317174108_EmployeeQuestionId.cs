﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OES.Data.Migrations
{
    public partial class EmployeeQuestionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeId_QuestionId",
                table: "EmployeeQuestions");

            migrationBuilder.AddColumn<Guid>(
                name: "EmployeeQuestionId",
                table: "EmployeeQuestions",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeId_EmployeeQuestionId_QuestionId",
                table: "EmployeeQuestions",
                columns: new[] { "EmployeeId", "EmployeeQuestionId", "QuestionId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeId_EmployeeQuestionId_QuestionId",
                table: "EmployeeQuestions");

            migrationBuilder.DropColumn(
                name: "EmployeeQuestionId",
                table: "EmployeeQuestions");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_EmployeeQuestions_EmployeeId_QuestionId",
                table: "EmployeeQuestions",
                columns: new[] { "EmployeeId", "QuestionId" });
        }
    }
}
