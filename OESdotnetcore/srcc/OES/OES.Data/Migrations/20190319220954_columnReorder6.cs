﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OES.Data.Migrations
{
    public partial class columnReorder6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeQuestions_Employees_EmployeeId",
                table: "EmployeeQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeQuestions_Questions_QuestionId",
                table: "EmployeeQuestions");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeQuestions_EmployeeId",
                table: "EmployeeQuestions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_EmployeeQuestions_EmployeeId",
                table: "EmployeeQuestions",
                column: "EmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeQuestions_Employees_EmployeeId",
                table: "EmployeeQuestions",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeQuestions_Questions_QuestionId",
                table: "EmployeeQuestions",
                column: "QuestionId",
                principalTable: "Questions",
                principalColumn: "QuestionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
