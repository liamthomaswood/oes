﻿using Microsoft.AspNetCore.Identity;

namespace OES.Data.Entities
{
    // Add profile data for application users by adding properties to the OESWebUser class
    public class OESWebUser : IdentityUser
    {
    }
}
