﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OES.Data.Entities;
using OES.Core.Models;

namespace OES.Data.Infrastructure
{
    public class OESContext : IdentityDbContext<OESWebUser>
    {
        public OESContext(DbContextOptions<OESContext> options)
            : base(options)
        {
        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeQuestion> EmployeeQuestions { get; set; }
        public DbSet<Question> Questions { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
