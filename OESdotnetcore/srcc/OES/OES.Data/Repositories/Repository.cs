﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OES.Core.Gateways.Interfaces;
using OES.Data.Infrastructure;

namespace OES.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly OESContext _context;

        public Repository(OESContext context)
        {
            _context = context;
        }

        public void Create(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
        }

        public void Delete(params object[] key)
        {
            var entityToDelete = _context.Set<TEntity>().Find(key);
            if(entityToDelete != null)
            {
                _context.Set<TEntity>().Remove(entityToDelete);
                _context.SaveChanges();
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().AsNoTracking();
        }

        private TEntity GetByKey(params object[] key)
        {
            return _context.Set<TEntity>().Find(key);
        }

        public TEntity GetEntity(TEntity entity)
        {
            return _context.Set<TEntity>().Find(entity);
        }

        public void Update(TEntity entity)
        {
            var dbEntityEntry = _context.Entry(entity);
            var keyProperties = dbEntityEntry.Entity.GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(KeyAttribute)));
            var keyObjects = keyProperties.Select(k => k.GetValue(dbEntityEntry.Entity)).ToArray();

            if (dbEntityEntry.State == EntityState.Detached)
            {
                var attachedEntity = GetByKey(keyObjects);

                if (attachedEntity != null)
                {
                    var attachedEntry = _context.Entry(attachedEntity);
                    attachedEntry.CurrentValues.SetValues(entity);
                }
                else
                {
                    _context.Set<TEntity>().Attach(entity);
                    dbEntityEntry.State = EntityState.Modified;
                }
            }
            _context.SaveChanges();
        }
    }
}
