﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OES.Core;
using OES.Core.Builders;
using OES.Core.Builders.Interfaces;
using OES.Core.Configuration;
using OES.Core.Gateways.Interfaces;
using OES.Core.Handlers;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using OES.Core.Services;
using OES.Core.Services.Interfaces;
using OES.Core.ViewModels;
using OES.Data.Entities;
using OES.Data.Infrastructure;
using OES.Data.Repositories;
using OES.SurveyApp.Handlers;
using System.IO;

namespace OES.SurveyApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<Config>(Configuration.GetSection("Smtp"));

            services.AddMvc(options =>
            {
                options.MaxModelValidationErrors = 50;
                options.ModelBindingMessageProvider.SetValueMustNotBeNullAccessor(
                    (_) => "The field is required.");
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<OESContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("OESContextConnection")));

            services.AddDefaultIdentity<OESWebUser>()
            .AddRoles<IdentityRole>()
            .AddDefaultUI()
            .AddEntityFrameworkStores<OESContext>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Admin", policy => policy.Requirements.Add(new Core.Models.AdminRequirement("Admin")));
            });

            services.AddSingleton<IAuthorizationHandler, Handlers.AdminRequirementHandler>();

            //Add Repos
            services.AddScoped<IRepository<Employee>, Repository<Employee>>();
            services.AddScoped<IRepository<Question>, Repository<Question>>();
            services.AddScoped<IRepository<EmployeeQuestion>, Repository<EmployeeQuestion>>();
            services.AddScoped<IRepository<Answer>, Repository<Answer>>();
            //Add Services
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IQuestionService, QuestionService>();
            services.AddScoped<IQuestionAssignmentService, QuestionAssignmentService>();
            services.AddScoped<IAnswerService, AnswerService>();
            services.AddScoped<IEmployeeQuestionAnswerService, EmployeeQuestionAnswerService>();
            services.AddScoped<IAdminReminderService, AdminReminderService>();
            services.AddScoped<IEmployeeQuestionService, EmployeeQuestionService>();
            services.AddScoped<IAccountEmailService, AccountEmailService>();
            //Add Handlers
            //services.AddScoped<IEmailHandler, EmailHandler>();
            //Add builders
            services.AddScoped<IEmailBuilder, EmailBuilder>();
            //Add Loggers and claims
            services.AddScoped<IUserClaimsPrincipalFactory<OESWebUser>, UserClaimsPrincipalFactory<OESWebUser, IdentityRole>>();
            //Email Configuraiton
            services.AddSingleton<Config>();
            services.AddSingleton<IEmailHandler, EmailHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
