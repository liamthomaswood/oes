﻿$(document).ready(function () {
    if ($("input:checked").prop("checked") && $("input:checked").val() !== "FreeText"){
        $("#question-choices").show();
        $("#question-choice-header").show();
    }
});

function DeleteQuestion(questionToDelete) {
    var questionToDeleteId = questionToDelete.id;
    vex.dialog.confirm({
        message: 'Are you sure you want to delete this question?',
        callback: function (value) {
            if (value) {
                $.post("DeleteQuestion", {
                    questionId: questionToDeleteId
                },
                function (data)
                {
                    vex.dialog.confirm({
                        message: data.message,
                        callback: function () {
                            window.location.reload();
                            $('.vex-dialog-button-secondary').hide();
                        }
                    });
                });
            }
        }
    });
}

function ShowHideAvailableOptions(selection) {
    if (selection !== "FreeText") {
        $("#question-choices").show();
        $("#question-choice-header").show();
    }
    else {
        $("#question-choices").hide();
        $("#question-choice-header").hide();
        $("#question-choices").val("")
    }
}