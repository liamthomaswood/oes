﻿function EditEmployee(employee) {
    $("#Employee_EmployeeId").val(employee.id);
    $("#edit-employee-form").show();
    $("#add-employee-form").hide();
    $("#add-employee").hide();
    var email = $(employee).parent().find(".employee-email").text();
    $("#EmployeeToEdit_EmailAddress").text(email);
    var name = $(employee).parent().find(".firstname-lastname").text();
    $("#EmployeeToEdit_FirstName").text(name.split(' ').slice(0, -1).join(' '));
    $("#EmployeeToEdit_LastName").text(name.split(' ').slice(-1).join(' '));
}

function ToggleAddEmployee(shouldShow) {
    $("#add-employee-form").show();
    $("#add-employee").hide();
    $("edit-employee-form").hide();
}

function CancelAddEdit() {
    $("#add-employee-form").hide();
    $("#add-employee").show();
    $("#edit-employee-form").hide();
    $("#EmployeeToAdd_FirstName").text("");
    $("#EmployeeToAdd_LastName").text("");
    $("#EmployeeToAdd_EmailAddress").text("");
    $("#EmployeeToEdit_FirstName").text("");
    $("#EmployeeToEdit_LastName").text("");
    $("#EmployeeToEdit_EmailAddress").text("");
}

function DeleteEmployee(employeeToDeleteId) {
    vex.dialog.confirm({
        message: 'Are you sure you want to delete this employee?',
        callback: function (value) {
            if (value) {
                $.post("DeleteEmployee", {
                    employeeId: employeeToDeleteId
                },
                    function (data) {
                        vex.dialog.confirm({
                            message: data.message,
                            callback: function () {
                                window.location.reload();
                                $('.vex-dialog-button-secondary').hide();
                            }
                        });
                    });
            }
        }
    });
}

function ResetPassword(employeeId) {
    vex.dialog.confirm({
        message: 'Are you sure you want to reset this password?',
        callback: function (value) {
            if (value) {
                $.post("ResetPassword", {
                    employeeId: employeeId
                },
                    function (data) {
                        vex.dialog.confirm({
                            message: data.message,
                            callback: function () {
                                window.location.reload();
                                $('.vex-dialog-button-secondary').hide();
                            }
                        });
                    });
            }
        }
    });
}