﻿$(document).ready(function () {
    $("#min-date").datepicker({ dateFormat: 'dd-mm-yy'});
    $("#max-date").datepicker({ dateFormat: 'dd-mm-yy'});
});

$('table').tablesorter({
    sortList: [[0, 1]]
});

function SearchAnswers() {
    if (($('#min-date').val() === "" && !$("max-date").val() === null) || !$('#min-date').val() === "" && $("max-date").val() === ""){
        vex.dialog.alert("One of the dates are missing, please check and try again");
        return false;
    }
    $("#filter-options").submit();
}

function ClearAnswerSearch() {
    $('#min-date').val() === null;
    $('#max-date').val() === null;
    $('#answerSearch').val() === null;
    form.submit();
}