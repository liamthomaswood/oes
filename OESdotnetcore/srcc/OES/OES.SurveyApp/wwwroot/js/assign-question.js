﻿function SendReminderEmail(employeeQuestion) {
    var employeeQuestionId = employeeQuestion.id;
    $.post("SendReminder", {
        employeeQuestionId: employeeQuestionId
    },
    function (data) {
        vex.dialog.confirm({
            message: data.message,
            callback: function () {
                window.location.reload();
                $('.vex-dialog-button-secondary').hide();
            }
        });
    });
}

function UnassignQuestion(employeeQuestion) {
    var employeeQuestionId = employeeQuestion.value;
    $.post("UnassignEmployeeQuestion", {
        employeeQuestionId: employeeQuestionId
    },
        function (data) {
            vex.dialog.confirm({
                message: data.message,
                callback: function () {
                    window.location.reload();
                    $('.vex-dialog-button-secondary').hide();
                }
            });
        });
}