﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OES.Core.Enumerations;
using OES.Core.Helpers;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using OES.Core.ViewModels;
using OES.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OES.SurveyApp.Controllers
{
    [Authorize(Policy = "Admin")]
    public class AdminController : Controller
    {
        private readonly IAdminReminderService _adminService;
        private readonly IQuestionService _questionService;
        private readonly IQuestionAssignmentService _questionAssignmentService;
        private readonly IEmployeeQuestionService _employeeQuestionService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeQuestionAnswerService _employeeQuestionAnswerService;
        private readonly IAccountEmailService _adminEmailService;
        private readonly UserManager<OESWebUser> _userManager;

        public AdminController
            (UserManager<OESWebUser> userManager, 
            IAdminReminderService adminService,
            IQuestionService questionService,
            IQuestionAssignmentService questionAssignmentService,
            IEmployeeQuestionService employeeQuestionService,
            IEmployeeService employeeService,
            IEmployeeQuestionAnswerService employeeQuestionAnswerService,
            IAccountEmailService adminEmailService)
        {
            _userManager = userManager;
            _adminService = adminService;
            _questionService = questionService;
            _employeeService = employeeService;
            _questionAssignmentService = questionAssignmentService;
            _employeeQuestionService = employeeQuestionService;
            _employeeQuestionAnswerService = employeeQuestionAnswerService;
            _adminEmailService = adminEmailService;
        }

        //Questions
        public IActionResult Question([Bind("search,questionFormat")]string search = null, string questionFormat = null)
        {
            var model = new AdminQuestionViewModel
            {
                QuestionList = _questionService.GetQuestions(search, questionFormat).ToList(),
                QuestionCategories = new List<QuestionCategory>
                {
                    QuestionCategory.Radio,
                    QuestionCategory.CheckBox,
                    QuestionCategory.FreeText
                }
            };

            if(TempData["AddStatus"] != null)
            {
                ViewBag.AddStatus = TempData["AddStatus"];
            } 

            return View(model);
        }

        public IActionResult AddQuestion([Bind("QuestionId")] string questionId)
        {
            var model = new AdminQuestionViewModel();
            if(questionId != null)
            {
                model.Question = _questionService.GetQuestions().SingleOrDefault(q => q.QuestionId == Guid.Parse(questionId));
            }

            model.QuestionCategories = new List<QuestionCategory>
                {
                    QuestionCategory.Radio,
                    QuestionCategory.CheckBox,
                    QuestionCategory.FreeText
                };
            return View(model);
        }

        [HttpPost]
        public IActionResult AddQuestion([Bind("Text,QuestionId,ResponseCategory,AvailableChoices")] Question question)
        {
            if (ModelState.IsValid)
            {
                string result;
                if(question.QuestionId == Guid.Empty || question.QuestionId == null)
                {
                    result = _questionService.AddQuestion(question);
                }
                else
                {
                    result = _questionService.EditQuestion(question.QuestionId, question.Text, question.ResponseCategory.ToString(), question.AvailableChoices);
                }
                TempData["AddStatus"] = result;
                return RedirectToAction("Question");
            }

            return RedirectToAction("Question");
        }

        [HttpPost]
        public JsonResult DeleteQuestion([Bind("QuestionId")] Guid questionId)
        {
            var result = _questionService.DeleteQuestion(questionId);

            return Json(new { success = true, message = result });
        }

        //Assign Questions
        public IActionResult AssignQuestion([Bind("FilteredDepartment,Search,Category,From,To")] Department filteredDepartment, string search, QuestionCategory questionCategory, string from, string to)
        {
            if(TempData["AssignStatus"] != null)
            {
                ViewBag.AssignStatus = TempData["AssignStatus"];
            }

            var model = new AssignQuestionViewModel
            {
                EmployeeQuestionViewModel = _employeeQuestionService.GetAssignedQuestionsToEmployees(filteredDepartment, search, questionCategory, from, to),
                From = from,
                To = to
            };
            return View(model);
        }

        public IActionResult AssignQuestionToEmployee([Bind("FilteredDepartment,Search,Category")] Department filteredDepartment, string search, QuestionCategory category)
        {
            var model = new AssignQuestionViewModel
            {
                EmployeeSelectList = new SelectList(_employeeService.FetchEmployees(filteredDepartment, search), nameof(Employee.EmployeeId), nameof(Employee.EmailAddress)),
                QuestionsSelectList = new SelectList(_questionService.GetQuestions(search), nameof(OES.Core.Models.Question.QuestionId), nameof(OES.Core.Models.Question.Text)),
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult AssignQuestionToEmployee([Bind("EmployeeIds, QuestionIds")] List<Guid> EmployeeIds, List<Guid> QuestionIds)
        {
            if (ModelState.IsValid)
            {
                var requestUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
                var result = _questionAssignmentService.AssignQuestionsToEmployeesMultiple(QuestionIds, EmployeeIds, requestUrl);
                TempData["AssignStatus"] = result.ResponseMessage;
                return RedirectToAction("AssignQuestion");
            }
            return RedirectToAction("AssignQuestion");
        }

        [HttpPost]
        public JsonResult SendReminder([Bind("EmployeeQuestionId")] string employeeQuestionId)
        {
            var requestUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            var result = _adminService.SendReminderEmail(Guid.Parse(employeeQuestionId), requestUrl);

            return Json(new { success = true, message = result });
        }

        [HttpPost]
        public JsonResult UnassignEmployeeQuestion([Bind("EmployeeQuestionId")] Guid employeeQuestionId)
        {
            var result = _questionAssignmentService.UnassignQuestion(employeeQuestionId);

            return Json(new { success = true, message = result });
        }

        //Employee Management
        public IActionResult EmployeeManagement([Bind("FilteredDeparment,Search")] Department filteredDepartment, string search = null)
        {
            if (TempData["AddStatus"] != null)
            {
                ViewBag.AddStatus = TempData["AddStatus"];
            }

            var model = new EmployeeViewModel
            {
                EmployeeList = _employeeService.FetchEmployees(filteredDepartment, search)
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewEmployee([Bind("FirstName,LastName,EmailAddress,Role,Department")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                var user = new OESWebUser { UserName = employee.EmailAddress, Email = employee.EmailAddress };
                var appUser = new Employee
                {
                    EmployeeId = Guid.Parse(user.Id),
                    EmailAddress = employee.EmailAddress,
                    Role = employee.Role,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    Department = employee.Department
                };
                _employeeService.CreateNewEmployee(appUser);

                var options = _userManager.Options.Password;
                var userPassword = PasswordHelpers.GeneratePassword(options);
                var result = await _userManager.CreateAsync(user, userPassword);

                await _userManager.AddClaimAsync(user, new Claim("Base User", "Base User"));
                if (result.Succeeded)
                {

                    var requestUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
                    _adminEmailService.SendNewSignUpEmail(appUser.FirstName, appUser.EmailAddress, userPassword, requestUrl);

                    TempData["AddStatus"] = "Employee Added";
                    return RedirectToAction("EmployeeManagement");
                }
            }
            TempData["AddStatus"] = "Sorry, the employee couldn't be added";
            return RedirectToAction("EmployeeManagement");
        }

        public IActionResult EditEmployee(string employeeId)
        {          
            if(TempData["AddStatus"] != null)
            {
                ViewBag.AddStatus = TempData["AddStatus"];
            }
            var employee = _employeeService.FetchEmployees().FirstOrDefault(e => e.EmployeeId.ToString() == employeeId);
            var model = new EmployeeEditModel {
                EmployeeId = employee.EmployeeId.ToString(),
                EmailAddress = employee.EmailAddress,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                LastEmail = employee.EmailAddress,
                Department = employee.Department,
                Role = employee.Role
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditEmployee([Bind("EmployeeId,FirstName,LastName,EmailAddress,LastEmail,Role,Department")] EmployeeEditModel employeeEditModel)
        {
            if (ModelState.IsValid)
            {
                var employee = new Employee
                {
                    EmployeeId = Guid.Parse(employeeEditModel.EmployeeId),
                    FirstName = employeeEditModel.FirstName,
                    LastName = employeeEditModel.LastName,
                    EmailAddress = employeeEditModel.EmailAddress,
                    Role = employeeEditModel.Role,
                    Department = employeeEditModel.Department
                };

                var userToUpdate = await _userManager.FindByEmailAsync(employeeEditModel.LastEmail);
                var stamp = await _userManager.GetSecurityStampAsync(userToUpdate);

                userToUpdate.Email = employeeEditModel.EmailAddress;
                userToUpdate.NormalizedEmail = employeeEditModel.EmailAddress;
                userToUpdate.UserName = employeeEditModel.EmailAddress;
                userToUpdate.NormalizedUserName = employeeEditModel.EmailAddress;
                userToUpdate.SecurityStamp = stamp;

                var update = await _userManager.UpdateSecurityStampAsync(userToUpdate);
                await _userManager.AddClaimAsync(userToUpdate, new Claim("Base User", "Base User"));
                await _userManager.UpdateAsync(userToUpdate);


                _employeeService.EditEmpoyee(employee);
                TempData["AddStatus"] = "Employee Details Updated";
            }
            return RedirectToAction("EmployeeManagement", new { employeeId = employeeEditModel.EmployeeId });

        }

        [HttpPost]
        public async Task<JsonResult> DeleteEmployee([Bind("EmployeeId")] string employeeId)
        {
            var result = _employeeQuestionAnswerService.DeleteEmployeeData(employeeId);
            var employee = await _userManager.FindByIdAsync(employeeId);

            await _userManager.DeleteAsync(employee);

            return Json(new { success = true, message = result });
        }

        public IActionResult Answers([Bind("search,from,to")]string search = null, string from = null, string to = null)
        {
            var model = new EmployeeAnswerModel
            {
                EmployeeQuestionAnswerModel = _employeeQuestionAnswerService.GetEmployeeQuestionAnswers(search, from, to),
                From = from,
                To = to
            };
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> ResetPassword([Bind("EmployeeId")] string employeeId)
        {
            var employee = _employeeService.FetchEmployees().FirstOrDefault(e => e.EmployeeId.ToString() == employeeId);
            var user = await _userManager.FindByIdAsync(employeeId);
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var password = PasswordHelpers.GeneratePassword(_userManager.Options.Password);

            await _userManager.ResetPasswordAsync(user, token, password);

            var requestUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            var result = _adminEmailService.SendPasswordResetEmail(employee.FirstName, employee.EmailAddress, password, requestUrl);

            return Json(new { success = true, message = result });
        }
    }
}