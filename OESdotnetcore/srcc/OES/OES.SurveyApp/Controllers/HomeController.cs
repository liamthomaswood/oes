﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OES.Core.Services.Interfaces;
using OES.Core.ViewModels;
using System.Diagnostics;
using System.Linq;

namespace OES.SurveyApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IAdminReminderService _adminService;
        private readonly IQuestionService _questionService;

        public HomeController(
            IAdminReminderService adminService,
            IQuestionService questionService)
        {
            _adminService = adminService;
            _questionService = questionService;
        }

        public IActionResult Index()
        {
            var questions = _questionService.GetQuestions().ToList();
            return View(questions);
        }

        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
