﻿using Microsoft.AspNetCore.Mvc;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using System;
using System.Security.Claims;

namespace OES.SurveyApp.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeQuestionAnswerService _employeeQuestionAnswerService;
        private readonly IAnswerService _answerService;

        public EmployeeController(IEmployeeQuestionAnswerService employeeQuestionAnswerService, IAnswerService answerService)
        {
            _employeeQuestionAnswerService = employeeQuestionAnswerService;
            _answerService = answerService;
        }

        public IActionResult EmployeeQuestions()
        {
            var model = _employeeQuestionAnswerService.GetEmployeeQuestionViewModel(Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            if (TempData["AddStatus"] != null)
            {
                ViewBag.AddStatus = TempData["AddStatus"];
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult EmployeeQuestions([Bind("QuestionId,Value")]  Answer answer)
        {
            if (ModelState.IsValid)
            {
                answer.Value = string.IsNullOrWhiteSpace(Request.Form["answer_input"].ToString()) ? answer.Value : Request.Form["answer_input"].ToString();
                _answerService.AnswerQuestion(answer, Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            }

            TempData["AddStatus"] = "Answer logged, thank you";

            return RedirectToAction("/EmployeeQuestions");
        }

        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ResetPassword([Bind("EmployeeId,NewPassword")] Guid emplyeeId, string password)
        {
            return RedirectToAction("EmployeeQuestions");
        }
    }
}