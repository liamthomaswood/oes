﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OES.Core.Enumerations;
using OES.Core.Helpers;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using OES.Core.ViewModels;
using OES.Data.Entities;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OES.SurveyApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<OESWebUser> _signInManager;
        private readonly UserManager<OESWebUser> _userManager;
        private readonly ILogger<Registration> _logger;
        private readonly IEmployeeService _employeeService;
        private readonly IAccountEmailService _adminEmailService;

        public AccountController(
            UserManager<OESWebUser> userManager,
            SignInManager<OESWebUser> signInManager,
            ILogger<Registration> logger,
            IEmailSender emailSender,
            IEmployeeService employeeService,
            IAccountEmailService adminEmailService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _employeeService = employeeService;
            _adminEmailService = adminEmailService;
        }

        //public IActionResult Register()
        //{
        //    var registerModel = new Registration();
        //    return View(new Registration());
        //}

        //[HttpPost]
        //public async Task<IActionResult> Register([Bind("FirstName,LastName,Email, Telephone, Password,ConfirmPassword")] Registration newUser)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new OESWebUser { UserName = newUser.Email, Email = newUser.Email };
        //        var appUser = new Employee
        //        {
        //            EmployeeId = Guid.Parse(user.Id),
        //            EmailAddress = newUser.Email,
        //            Telephone = newUser.Telephone,
        //            Role = Roles.BaseUser,
        //            FirstName = newUser.FirstName,
        //            LastName = newUser.LastName
        //        };
        //        _employeeService.CreateNewEmployee(appUser);
        //        var result = await _userManager.CreateAsync(user, newUser.Password);
        //        await _userManager.AddClaimAsync(user, new Claim("Admin", "Admin"));

        //        if (result.Succeeded)
        //        {
        //            _logger.LogInformation("User created a new account with password.");
        //            var requestUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
        //            _adminEmailService.SendNewSignUpEmail(appUser.FirstName, appUser.EmailAddress, newUser.Password, requestUrl);

        //            ViewBag.AddStatus = "Thank you, your sign up has been completed. A copy of your credentials has been emailed.";
        //            return View("Register");
        //        }
        //    }
        //    return RedirectToAction("Index");
        //}

        public async Task<IActionResult> Login()
        {
            if (TempData["AddStatus"] != null)
            {
                ViewBag.AddStatus = TempData["AddStatus"];
            }

            if (User.Identity.IsAuthenticated)
            {
                var claims = await _userManager.GetClaimsAsync(_userManager.FindByEmailAsync(User.Identity.Name).Result);

                var hasAdminClaim = claims.Any(cl => cl.Value == "Admin");

                if (hasAdminClaim)
                {
                    return RedirectToAction("Question", "Admin");
                }

                return RedirectToAction("EmployeeQuestions", "Employee");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([Bind("Email, Password")] Login login)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(login.Email, login.Password, login.RememberMe, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");

                    var claims = await _userManager.GetClaimsAsync(_userManager.FindByEmailAsync(login.Email).Result);

                    var hasAdminClaim = claims.Any(cl => cl.Value == "Admin");

                    if (hasAdminClaim)
                    {
                        return RedirectToAction("Question", "Admin");
                    }

                    return RedirectToAction("EmployeeQuestions", "Employee");
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToPage("./Lockout");
                }
                else
                {
                    TempData["AddStatus"] = $"Sorry, username/password not recognized.";
                    return RedirectToAction("Login");
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public IActionResult ResetPassword()
        {
            if(TempData["AddStatus"] != null)
            {
                ViewBag.AddStatus = TempData["AddStatus"];
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword([Bind("EmployeeEmail")] string employeeEmail)
        {
            try
            {
                var employee = _employeeService.GetEmployeeByEmail(employeeEmail);
                var user = await _userManager.FindByIdAsync(employee.EmployeeId.ToString());
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var password = PasswordHelpers.GeneratePassword(_userManager.Options.Password);
                await _userManager.ResetPasswordAsync(user, token, password);

                var requestUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
                var result = _adminEmailService.SendPasswordResetEmail(employee.FirstName, employee.EmailAddress, password, requestUrl);
                TempData["AddStatus"] = 
                    $@"Your password has been reset. 
                      An email with your new password has been sent.";
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                TempData["AddStatus"] = $"Sorry, {employeeEmail} wasn't found in our records";
                return RedirectToAction("ResetPassword");
            }
        }

        public IActionResult EmployeeResetPassword()
        {
            if (TempData["AddStatus"] != null)
            {
                ViewBag.AddStatus = TempData["AddStatus"];
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeResetPassword([Bind("EmployeeId,Password,ConfirmPassword,CurrentPassword")] ResetPasswordModel passwordModel)
        {
            if (ModelState.IsValid)
            {
                var employee = _employeeService.GetEmployeeByEmail(User.Identity.Name);
                var user = await _userManager.FindByIdAsync(employee.EmployeeId.ToString());
                var passwordMatches = await _userManager.CheckPasswordAsync(user, passwordModel.CurrentPassword);
                if (passwordMatches)
                {
                    var result = await _userManager.ChangePasswordAsync(user, passwordModel.CurrentPassword, passwordModel.ConfirmPassword);
                    if (result.Succeeded)
                    {
                        TempData["AddStatus"] = "Password changed successfully";
                    }
                    else
                    {
                        TempData["AddStatus"] = result.Errors.First();
                    }
                }
                else
                {
                    TempData["AddStatus"] = 
                        @"Sorry, an error occurred when attempting to change the password.
                          Please try again, or contact <a>Support</a> if the problem persists";
                }
            }
                    return RedirectToAction("Login");
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}