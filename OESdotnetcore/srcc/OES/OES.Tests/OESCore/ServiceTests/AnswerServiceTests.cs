﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OES.Core.Configuration;
using OES.Core.Handlers;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using OES.Core.Services;
using OES.Core.Services.Interfaces;
using OES.Tests.OESCore.MockRepositories;
using System.Linq;

namespace OES.Tests.OESCore.ServiceTests
{
    [TestClass]
    public class AnswerServiceTests
    {
        private readonly MockRepository<Answer> _mockAnswerRepo;
        private readonly MockRepository<EmployeeQuestion> _mockEmployeeQuestionRepo;
        private readonly MockRepository<Employee> _mockEmployeeRepo;
        private readonly IAnswerService _answerService;
        private readonly IEmailHandler _emailService;
        private readonly IEmployeeService _employeeService;
        private readonly IOptions<Config> config;
        private readonly IHostingEnvironment env;

        public AnswerServiceTests()
        {
            _mockAnswerRepo = MockAnswerRepo.GetMockAnswerRepo();
            _mockEmployeeQuestionRepo = MockQuestionRepo.GetMockEmployeeQuestionRepo();
            _mockEmployeeRepo = MockQuestionRepo.GetMockEmployeeRepo();

            _employeeService = new EmployeeService(_mockEmployeeRepo);
            _emailService = new EmailHandler(config, env);
            _answerService = new AnswerService(_mockAnswerRepo, _mockEmployeeQuestionRepo, _emailService, _employeeService);            
        }

        [TestMethod]
        [TestCategory("OES.AnswerService Tests")]
        public void AnswerQuestion_CreatesNewAnswer()
        {
            var originalCount = _mockAnswerRepo.GetAll().Count();
            var answer = new Answer() { QuestionId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().QuestionId };
            var result = _answerService.AnswerQuestion(answer, _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId);
            Assert.AreEqual(originalCount + 1, _mockAnswerRepo.GetAll().Count());
        }

        //answer question updates eq object. sets answered to 1
        [TestMethod]
        [TestCategory("OES.AnswerService Tests")]
        public void AnswerQuestion_SetsEmployeeQuestionAnsweredToTrue()
        {
            var eq = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault(x => x.Answered == false);
            var orignalAnswered = eq.Answered;
            var answer = new Answer() { QuestionId = eq.QuestionId };

            var result = _answerService.AnswerQuestion(answer, eq.EmployeeId);
            var updatedEq = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault(x => x.EmployeeQuestionId == eq.EmployeeQuestionId).Answered;

            Assert.IsFalse(orignalAnswered);
            Assert.IsTrue(updatedEq);

        }
    }
}
