﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OES.Core.Models;
using OES.Core.Services;
using OES.Core.Services.Interfaces;
using OES.Tests.OESCore.MockRepositories;

namespace OES.Tests.OESCore.ServiceTests
{
    [TestClass]
    public class QuestionServiceTests
    {
        private readonly MockRepository<Question> _mockQuestionRepository;
        private readonly MockRepository<Employee> _employeeRepo;
        private readonly MockRepository<EmployeeQuestion> _employeeQuestionRepo;
        private readonly IQuestionService _mockQuestionService;

        public QuestionServiceTests()
        {
            _mockQuestionRepository = MockQuestionRepo.GetMockQuestionRepo();
            _employeeRepo = MockEmployeeRepo.GetMockEmployeeRepo();
            _employeeQuestionRepo = MockEmployeeQuestionRepo.GetMockEmployeeQuestionRepo();
            _mockQuestionService = new QuestionService(_mockQuestionRepository, _employeeQuestionRepo);
        }

        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void AddQuestion_AddsQuestionToRepository()
        {
            var newQuestion = new Question();
           
            var result = _mockQuestionService.AddQuestion(newQuestion);

            Assert.AreEqual("Question added successfully", result);
        }

        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void AddQuestion_IncrementsRepo()
        {
            var expected = _mockQuestionRepository.GetAll().ToList().Count + 1;
            var newQuestion = new Question();

            _mockQuestionService.AddQuestion(newQuestion);
            var result = _mockQuestionRepository.GetAll().ToList().Count;

            Assert.AreEqual(expected, result);
        }


        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void AddQuestion_DoesntAddDuplicatedQuestion()
        {
            var newQuestion = new Question { QuestionId = Guid.NewGuid() };

            _mockQuestionService.AddQuestion(newQuestion);
            var result = _mockQuestionService.AddQuestion(newQuestion);

            Assert.AreEqual("The question already exists, please double check and try again.", result);
        }

        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void GetQuestionsForEmployee_ReturnsCorrectQuestionsForEmployee()
        {
            //create an employee
            var employee = new Employee { EmployeeId = Guid.Parse("cfef6dcc-7d3d-4fb0-9c78-78038a4217f7"), EmailAddress = "the_dank_lord@mail.com" };
            //assign a question to that employee
            _employeeQuestionRepo.Create(new EmployeeQuestion { EmployeeId = employee.EmployeeId, QuestionId = _mockQuestionRepository.GetAll().First().QuestionId });
            //get the qestions for that employee
            var employeesQuestions = _mockQuestionService.GetQuestionsForEmployee(employee.EmployeeId);

            Assert.AreEqual(employeesQuestions.Count(), 1);
        }

        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void GetAllQuestions_ReturnsAllQuestions()
        {
            var questionCount = _mockQuestionService.GetQuestions().Count();
            var actual = _mockQuestionRepository.GetAll().Count();
            Assert.AreEqual(questionCount, actual);
        }

        //Get unanswered questions for employee
        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void GetUnansweredQuestionsForEmployee_ReturnsCorrectQuestionsForEmployee()
        {
            //create an employee
            var employee = new Employee { EmployeeId = Guid.Parse("cfef6dcc-7d3d-4fb0-9c78-78038a4217f7"), EmailAddress = "the_dank_lord@mail.com" };
            //assign a question to that employee
            _employeeQuestionRepo.Create(new EmployeeQuestion { EmployeeId = employee.EmployeeId, QuestionId = _mockQuestionRepository.GetAll().First().QuestionId, Answered = false });
            //get the qestions for that employee
            var employeesQuestions = _mockQuestionService.GetUnansweredQuestionsForEmployee(employee.EmployeeId);

            Assert.AreEqual(employeesQuestions.Count(), 1);
        }

        //Get answered questions for employee
        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void GetAnsweredQuestionsForEmployee_ReturnsCorrectQuestionsForEmployee()
        {
            //create an employee
            var employee = new Employee { EmployeeId = Guid.Parse("cfef6dcc-7d3d-4fb0-9c78-78038a4217f7"), EmailAddress = "the_dank_lord@mail.com" };
            //assign a question to that employee
            _employeeQuestionRepo.Create(new EmployeeQuestion { EmployeeId = employee.EmployeeId, QuestionId = _mockQuestionRepository.GetAll().First().QuestionId, Answered = true });
            //get the qestions for that employee
            var employeesQuestions = _mockQuestionService.GetAnsweredQuestionsForEmployee(employee.EmployeeId);

            Assert.AreEqual(employeesQuestions.Count(), 1);
        }

        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        public void EditQuestion_UpdatesQuestionProperties()
        {
            var question = _mockQuestionService.GetQuestions().First();
            var questionId = question.QuestionId;
            var updatedText = "This is the new question";
            question.Text = updatedText;

            _mockQuestionService.EditQuestion(questionId, updatedText, question.ResponseCategory.ToString(), "");
            var questionAfter = _mockQuestionService.GetQuestions().FirstOrDefault(q => q.QuestionId == questionId).Text;

            Assert.AreEqual(updatedText, questionAfter);
        }

        [TestMethod]
        [TestCategory("OES.QuestionService Tests")]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DeleteQuestion_RemovesQuestionFromRepo()
        {
            var questionToDelete = _mockQuestionRepository.GetAll().First();
            var questionId = questionToDelete.QuestionId;
            _mockQuestionService.DeleteQuestion(questionId);
            var deletedQuestion = _mockQuestionRepository.GetAll().First(q => q.QuestionId == questionToDelete.QuestionId);
        }
    }
}
