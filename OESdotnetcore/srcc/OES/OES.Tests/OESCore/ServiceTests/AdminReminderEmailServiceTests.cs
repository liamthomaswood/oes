﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OES.Core.Builders.Interfaces;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using OES.Core.Services;
using OES.Core.Services.Interfaces;
using OES.Tests.OESCore.MockRepositories;
using System;
using System.Linq;

namespace OES.Tests.OESCore.ServiceTests
{
    [TestClass]
    public class AdminReminderEmailServiceTests
    {
        private readonly MockRepository<Employee> _mockEmployeeRepo;
        private readonly MockRepository<Question> _mockQuestionRepo;
        private readonly MockRepository<EmployeeQuestion> _mockEmployeeQuestionRepo;

        private readonly IAdminReminderService _adminService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmailHandler _emailService;
        private readonly IEmailBuilder _emailBuilder;
        private readonly IEmployeeQuestionService _employeeQuestionService;
        private readonly IAccountEmailService _adminEmailService;
        private readonly IQuestionService _questionService;

        public AdminReminderEmailServiceTests()
        {
            _mockEmployeeRepo = MockQuestionRepo.GetMockEmployeeRepo();
            _mockQuestionRepo = MockQuestionRepo.GetMockQuestionRepo();
            _mockEmployeeQuestionRepo = MockQuestionRepo.GetMockEmployeeQuestionRepo();

            _employeeService = new EmployeeService(_mockEmployeeRepo);
            _adminEmailService = new AccountEmailService(_emailService, _emailBuilder);
            _employeeQuestionService = new EmployeeQuestionService(_mockEmployeeQuestionRepo, _mockEmployeeRepo, _mockQuestionRepo, _questionService, _employeeService);
            _adminService = new AdminReminderService(_emailService, _employeeService, _employeeQuestionService, _emailBuilder);
        }

        [TestMethod]
        [TestCategory("OES.AdminService Tests")]
        public void SendReminderEmail_SendsReminder()
        {
            //var result = _adminService.SendReminderEmail(_employeeService.FetchAllEmployees().First().EmployeeId);
            //Assert.AreEqual(result, "Reminder email sent");
        }

        [TestMethod]
        [TestCategory("OES.AdminService Tests")]
        public void SendReminderEmail_IncrementsRemindersSentByOne()
        {
            //var eq = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault();
            //var eqRemindersBefore = eq.NumberReminders;
            //_adminService.SendReminderEmail(eq.EmployeeId);
            //var updatedEqAfter = eq.NumberReminders;

            //Assert.AreEqual(eqRemindersBefore + 1, updatedEqAfter);
        }

        [TestMethod]
        [TestCategory("OES.AdminService Tests")]
        public void SendReminderEmail_UpdatesLastReminderSentDate()
        {
            //var eq = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault();
            //eq.ReminderSent = DateTime.MinValue;
            //var lastUpdated = eq.ReminderSent;
            //_adminService.SendReminderEmail(eq.EmployeeId);
            //var updatedEqAfter = eq.ReminderSent > lastUpdated;

            //Assert.IsTrue(updatedEqAfter);
        }
    }
}
