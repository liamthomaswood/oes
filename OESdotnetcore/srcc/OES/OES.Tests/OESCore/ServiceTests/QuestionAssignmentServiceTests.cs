﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using OES.Core.Services;
using OES.Core.Services.Interfaces;
using OES.Tests.OESCore.MockRepositories;

namespace OES.Tests.OESCore.ServiceTests
{
    [TestClass]
    public class QuestionAssignmentServiceTests
    {
        private readonly MockRepository<EmployeeQuestion> _mockEmployeeQuestionRepo;
        private readonly MockRepository<Employee> _mockEmployeeRepo;
        private readonly IQuestionAssignmentService _questionAssignmentService;
        private readonly IAccountEmailService _adminEmailService;
        private readonly IEmailHandler _emailService;
        private readonly IEmployeeService _employeeService;

        public QuestionAssignmentServiceTests()
        {
            _mockEmployeeQuestionRepo = MockQuestionRepo.GetMockEmployeeQuestionRepo();
            _mockEmployeeRepo = MockQuestionRepo.GetMockEmployeeRepo();

            _employeeService = new EmployeeService(_mockEmployeeRepo);

            _questionAssignmentService = new QuestionAssignmentService(_mockEmployeeQuestionRepo, _adminEmailService, _employeeService);
        }

        [TestMethod]
        [TestCategory("OES.QuestionAssignmentService Tests")]
        public void AssignQuestionToEmployee_AddsNewQuestionToRepo()
        {
            var expected = _mockEmployeeQuestionRepo.GetAll().ToList().Count + 1;

            _questionAssignmentService.AssignQuestionToEmployee(new EmployeeQuestion{ EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId }, "", false);
            var result = _mockEmployeeQuestionRepo.GetAll().ToList().Count;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [TestCategory("OES.QuestionAssignmentService Tests")]
        public void AssignQuestionToEmployee_Fails_WhenDuplicateAdded()
        {
            var employeeQuestion = new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("dad67ace-cc1d-445d-bb3a-1749c4c2b870"), EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId, QuestionId = Guid.Parse("6fa670cf-5d70-45b5-b652-374e7e4c3370") };

            _questionAssignmentService.AssignQuestionToEmployee(employeeQuestion, "", false);
            var result = _questionAssignmentService.AssignQuestionToEmployee(employeeQuestion, "", false);

            Assert.AreEqual("Question has already been added", result);
        }

        [TestMethod]
        [TestCategory("OES.QuestionAssignmentService Tests")]
        public void AssignQuestionsToEmployeesMultiple_AddsMulipleEmployeeQuestion()
        {
            var firstEmployeeQuestion = new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("c19eeebf-32d5-4a02-bc83-f0797443f576"), EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId, QuestionId = Guid.Parse("a7bf9835-2482-4f89-86b5-022fbb0984da") };
            var secondEmployeeQuestion = new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("3f7295fb-f348-48d3-b13b-cf7cc8bff0e2"), EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId, QuestionId = Guid.Parse("6fa670cf-5d70-45b5-b652-374e7e4c3370") };

            var questionGuidList = new List<Guid>
            {
                firstEmployeeQuestion.QuestionId,
                secondEmployeeQuestion.QuestionId
            };

            var employeeGuidList = new List<Guid>
            {
                firstEmployeeQuestion.EmployeeId
            };

            var expected = _mockEmployeeQuestionRepo.GetAll().ToList().Count + 2;
            var result = _questionAssignmentService.AssignQuestionsToEmployeesMultiple(questionGuidList, employeeGuidList, "", false);
            var countResult = _mockEmployeeQuestionRepo.GetAll().ToList().Count;

            Assert.AreEqual(expected, countResult);
        }

        //If one's already been assigned then update the response message
        [TestMethod]
        [TestCategory("OES.QuestionAssignmentService Tests")]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AssignQuestionToEmployeesMultiple_CorrectlyAssignsButIgnoresDuplicates()
        {
            var eqId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeQuestionId;
            _questionAssignmentService.UnassignQuestion(eqId);
            _mockEmployeeQuestionRepo.GetAll().Single(x => x.EmployeeQuestionId == eqId); var firstEmployeeQuestion = new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("c19eeebf-32d5-4a02-bc83-f0797443f576"), EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId, QuestionId = Guid.Parse("6fa670cf-5d70-45b5-b652-374e7e4c3370") };
            var secondEmployeeQuestion = new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("3f7295fb-f348-48d3-b13b-cf7cc8bff0e2"), EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId, QuestionId = Guid.Parse("6fa670cf-5d70-45b5-b652-374e7e4c3370") };

            var questionGuidList = new List<Guid>
            {
                firstEmployeeQuestion.QuestionId,
                secondEmployeeQuestion.QuestionId
            };

            var employeeGuidList = new List<Guid>
            {
                firstEmployeeQuestion.EmployeeId
            };

            var result = _questionAssignmentService.AssignQuestionsToEmployeesMultiple(questionGuidList, employeeGuidList, "", false);
            var expected = $"{questionGuidList.Count} Questions already been assigned and no action taken.";

            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        [TestCategory("OES.QuestionAssignmentService Tests")]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AssignQuestionToEmployeesMultiple_UpdatesResponseMessage()
        {
            var eqId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeQuestionId;
            _questionAssignmentService.UnassignQuestion(eqId);
            _mockEmployeeQuestionRepo.GetAll().Single(x => x.EmployeeQuestionId == eqId); var firstEmployeeQuestion = new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("c19eeebf-32d5-4a02-bc83-f0797443f576"), EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId, QuestionId = Guid.Parse("6fa670cf-5d70-45b5-b652-374e7e4c3370") };
            var secondEmployeeQuestion = new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("3f7295fb-f348-48d3-b13b-cf7cc8bff0e2"), EmployeeId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeId, QuestionId = Guid.Parse("6fa670cf-5d70-45b5-b652-374e7e4c3370") };

            var questionGuidList = new List<Guid>
            {
                firstEmployeeQuestion.QuestionId,
                secondEmployeeQuestion.QuestionId
            };

            var employeeGuidList = new List<Guid>
            {
                firstEmployeeQuestion.EmployeeId
            };

            var expected = $"{questionGuidList.Count} Questions have now been assigned and an email sent.";
            var result = _questionAssignmentService.AssignQuestionsToEmployeesMultiple(questionGuidList, employeeGuidList, "", false);

            Assert.AreEqual(expected, result.ResponseMessage);
        }

        [TestMethod]
        [TestCategory("OES.QuestionAssignmentService Tests")]
        [ExpectedException(typeof(InvalidOperationException))]
        public void UnassignQuestion_DeletesEQFromRepo()
        {
            var eqId = _mockEmployeeQuestionRepo.GetAll().FirstOrDefault().EmployeeQuestionId;
            _questionAssignmentService.UnassignQuestion(eqId);
            _mockEmployeeQuestionRepo.GetAll().Single(x => x.EmployeeQuestionId == eqId);
        }
    }
}
