﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OES.Core.Models;
using OES.Core.Services;
using OES.Core.Services.Interfaces;
using OES.Tests.OESCore.MockRepositories;

namespace OES.Tests.OESCore.ServiceTests
{
    [TestClass]
    public class EmployeeServiceTests
    {
        private readonly MockRepository<Employee> _mockEmployeeRepo;
        private readonly IEmployeeService _employeeService;

        public EmployeeServiceTests()
        {
            _mockEmployeeRepo = MockEmployeeRepo.GetMockEmployeeRepo();
            _employeeService = new EmployeeService(_mockEmployeeRepo);
        }

        [TestMethod]
        [TestCategory("OES.EmployeeService Tests")]
        public void FetchAllEmployees_ReturnsAllEmployees()
        {
            var expected = _mockEmployeeRepo.GetAll().ToList().Count;
            var result = _employeeService.FetchEmployees().Count;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [TestCategory("OES.EmployeeService Tests")]
        public void FetchEmployeeByEmail_ReturnsCorrectEmployee_GivenCorrectEmail()
        {
            var expected = "woodliam003@gmail.com";
            var result = _employeeService.GetEmployeeByEmail("woodliam003@gmail.com").EmailAddress;

            Assert.AreEqual(expected, result);
        }

        //get all questions answered by employees

        //get all answers for employees
    }
}
