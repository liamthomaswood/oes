﻿using OES.Core.Gateways.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OES.Tests.OESCore.MockRepositories
{
    public class MockRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly List<TEntity> _entityList;

        public MockRepository()
        {
            _entityList = new List<TEntity>();
        }

        public void Create(TEntity entity)
        {
            _entityList.Add(entity);
        }

        public void Delete(params object[] key)
        {
            var entity = _entityList.First();
            _entityList.Remove(entity);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _entityList.AsQueryable();
        }

        public TEntity GetEntity(TEntity entity)
        {
            return entity;
        }

        public void Update(TEntity entity)
        {
            _entityList.Remove(entity);
            _entityList.Add(entity);
        }
    }
}
