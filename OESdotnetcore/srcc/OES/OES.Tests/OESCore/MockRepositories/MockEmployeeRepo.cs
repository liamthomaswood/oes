﻿using OES.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OES.Tests.OESCore.MockRepositories
{
    public static class MockEmployeeRepo
    {
        public static MockRepository<Employee> GetMockEmployeeRepo()
        {
            var repo = new MockRepository<Employee>();

            repo.Create(new Employee { EmployeeId = Guid.Parse("be340130-f84d-4d6c-8aff-3a113fa90c5a"), EmailAddress = "woodliam003@gmail.com" });
            repo.Create(new Employee { EmployeeId = Guid.Parse("8a4eca15-1cb4-459f-9b60-8d95baaccef0"), EmailAddress = "randy_lahey@gmail.com" });

            return repo;
        }

    }
}
