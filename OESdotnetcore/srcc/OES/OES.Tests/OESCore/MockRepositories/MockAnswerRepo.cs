﻿using OES.Core.Models;
using System;

namespace OES.Tests.OESCore.MockRepositories
{
    public static class MockAnswerRepo
    {
        public static MockRepository<Answer> GetMockAnswerRepo()
        {
            var repo = new MockRepository<Answer>();

            repo.Create(new Answer { AnswerId = Guid.Parse("a8f90e88-d770-49e5-b86b-8ee861ebac0d"), EmployeeId = Guid.Parse("36b7e6de-ba2e-463f-92cc-a3270c0581cb"), QuestionId = Guid.Parse("068dbea5-ea1a-4ffc-ac93-1e9a78923ef9"), Value = "Yes", TimeResponded = DateTime.UtcNow });

            return repo;
        }
    }
}
