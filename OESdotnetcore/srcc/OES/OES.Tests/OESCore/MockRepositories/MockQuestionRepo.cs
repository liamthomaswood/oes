﻿using OES.Core.Enumerations;
using OES.Core.Models;
using System;

namespace OES.Tests.OESCore.MockRepositories
{
    public static class MockQuestionRepo
    {
        public static MockRepository<Question> GetMockQuestionRepo()
        {
            var repo = new MockRepository<Question>();

            repo.Create(new Question { QuestionId = Guid.Parse("068dbea5-ea1a-4ffc-ac93-1e9a78923ef9"), Text = "Are you satisfied?", ResponseCategory = QuestionCategory.FreeText });
            repo.Create(new Question { QuestionId = Guid.Parse("49bfcf4c-230e-45ff-a8fd-94781897fd1f"), Text = "How often do you work hard?", ResponseCategory = QuestionCategory.Radio });

            return repo;
        }

        public static MockRepository<EmployeeQuestion> GetMockEmployeeQuestionRepo()
        {
            var repo = new MockRepository<EmployeeQuestion>();

            repo.Create(new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("be340130-f84d-4d6c-8aff-3a113fa90c5a"), QuestionId = Guid.Parse("068dbea5-ea1a-4ffc-ac93-1e9a78923ef9"), EmployeeId = Guid.Parse("be340130-f84d-4d6c-8aff-3a113fa90c5a"), Answered = false });
            repo.Create(new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("7597fddf-9a31-4744-a4cd-b09d7e92ab93"), QuestionId = Guid.Parse("49bfcf4c-230e-45ff-a8fd-94781897fd1f"), EmployeeId = Guid.Parse("8a4eca15-1cb4-459f-9b60-8d95baaccef0") });

            return repo;
        }

        public static MockRepository<Employee> GetMockEmployeeRepo()
        {
            var repo = new MockRepository<Employee>();

            repo.Create(new Employee { EmployeeId = Guid.Parse("be340130-f84d-4d6c-8aff-3a113fa90c5a"), EmailAddress = "woodliam003@gmail.com", Role = Core.Enumerations.Roles.AdminUser });
            repo.Create(new Employee { EmployeeId = Guid.Parse("8a4eca15-1cb4-459f-9b60-8d95baaccef0"), EmailAddress = "randy_lahey@gmail.com" });

            return repo;
        }

        public static MockRepository<Answer> GetMockAnswerRepo()
        {
            var repo = new MockRepository<Answer>();

            repo.Create(new Answer { AnswerId = Guid.Parse("a8f90e88-d770-49e5-b86b-8ee861ebac0d"), EmployeeId = Guid.Parse("be340130-f84d-4d6c-8aff-3a113fa90c5a"), QuestionId = Guid.Parse("068dbea5-ea1a-4ffc-ac93-1e9a78923ef9"), Value = "Yes", TimeResponded = DateTime.UtcNow });

            return repo;
        }
    }
}
