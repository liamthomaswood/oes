﻿using OES.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OES.Tests.OESCore.MockRepositories
{
    public static class MockEmployeeQuestionRepo
    {
        public static MockRepository<EmployeeQuestion> GetMockEmployeeQuestionRepo()
        {
            var repo = new MockRepository<EmployeeQuestion>();

            repo.Create(new EmployeeQuestion { EmployeeQuestionId = Guid.Parse("be340130-f84d-4d6c-8aff-3a113fa90c5a"), QuestionId = Guid.Parse("068dbea5-ea1a-4ffc-ac93-1e9a78923ef9"), EmployeeId = Guid.Parse("36b7e6de-ba2e-463f-92cc-a3270c0581cb") });

            return repo;
        }
    }
}
