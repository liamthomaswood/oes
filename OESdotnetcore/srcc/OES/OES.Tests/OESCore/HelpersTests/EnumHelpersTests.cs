﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OES.Tests.OESCore.HelpersTests
{
    [TestClass]
    public class EnumHelpersTests
    {
        [TestMethod]
        [TestCategory("OES.EnumHelpers Tests")]
        public void GetDeparmentName_ReturnsCorrectDepartment()
        {
            var departmentName = "AccountManagement";
            var result = departmentName.GetDepartmentName();
            Assert.AreEqual(result, "Account Management");
        }

        [TestMethod]
        [TestCategory("OES.EnumHelpers Tests")]
        public void GetDeparmentName_ReturnsCorrectError_GivenIncorrectDepartment()
        {
            var departmentName = "Not an account";
            var result = departmentName.GetDepartmentName().Contains($"Sorry, {departmentName} is not recognised");
            Assert.IsTrue(result);
        }
    }
}
