﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OES.App.Areas.Identity.Data;
using OES.App.Models;

[assembly: HostingStartup(typeof(OES.App.Areas.Identity.IdentityHostingStartup))]
namespace OES.App.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<OESContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("OESContextConnection")));

                services.AddDefaultIdentity<OESAppUser>()
                    .AddEntityFrameworkStores<OESContext>();
            });
        }
    }
}