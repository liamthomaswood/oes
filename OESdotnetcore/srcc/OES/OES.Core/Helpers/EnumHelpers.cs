﻿using OES.Core.Enumerations;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

public static class EnumHelpers
{
    public static string GetDepartmentName(this string enumValue)
    {
        Department parsedDepartment;
        Enum.TryParse(enumValue, true, out parsedDepartment);
        if(parsedDepartment != 0)
        {
            return parsedDepartment.GetType()
                .GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DescriptionAttribute>()
                .Description;
        }
        return $"Sorry, {enumValue} is not recognised as a valid department";
    }
}