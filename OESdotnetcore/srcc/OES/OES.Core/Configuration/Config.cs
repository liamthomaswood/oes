﻿using System.Collections.Generic;

namespace OES.Core.Configuration
{
    public class Config
    {
        public string SmtpHost { get; set; }
        public string SmtpUser { get; set; }
        public string SmtpPwd { get; set; }
        public List<string> EmailsForProcessFails { get; set; }
        public string EmailFrom { get; set; }
        public string DBConnection { get; set; }
    }
}
