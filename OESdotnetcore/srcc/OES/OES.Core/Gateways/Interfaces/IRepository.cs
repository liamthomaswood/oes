﻿using System.Linq;

namespace OES.Core.Gateways.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();

        TEntity GetEntity(TEntity entity);

        void Create(TEntity entity);

        void Update(TEntity entity);

        void Delete(params object[] key);
    }
}
