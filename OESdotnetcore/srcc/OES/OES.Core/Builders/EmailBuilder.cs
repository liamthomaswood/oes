﻿using OES.Core.Builders.Interfaces;
using OES.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace OES.Core.Builders
{
    public class EmailBuilder : IEmailBuilder
    {
        public Email BuildEmail(EmailContent emailContent)
        {
            var email = new Email();
            var templatesDirectroy = ToApplicationPath("Templates");

            StringBuilder body =
            new StringBuilder(
                File.ReadAllText(string.Format("{0}\\{1}.htm", templatesDirectroy,
                    emailContent.Template)));

            body = body.Replace("¬USER_FIRSTNAME", emailContent.FirstName);
            body = body.Replace("¬LOGIN_URL¬", emailContent.LoginUrl);
            body = body.Replace("¬PARTNER_EMAIL_LOGO¬", "https://oemweb.s3.amazonaws.com/files/a32d1fa9-5aa7-49ac-b7ff-67be331ac92f.png");
            body = body.Replace("¬PARTNER_TELEPHONE¬", "01483 492600");
            body = body.Replace("¬PARTNER_URL¬", "www.openenergymarket.com");
            body = body.Replace("¬PASSWORD¬", emailContent.Password);

            email.Body = body.ToString();
            email.From = new MailAddress("OESAdmin@openenergymarket.com", "Open Energy Survey");
            email.To = emailContent.Email;
            email.Subject = emailContent.Subject;

            return email;
        }

        private string ToApplicationPath(string fileName)
        {
            var exePath = Path.GetDirectoryName(System.Reflection
                                .Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;
            return Path.Combine(appRoot, fileName);
        }
    }
}
