﻿using OES.Core.Models;

namespace OES.Core.Builders.Interfaces
{
    public interface IEmailBuilder
    {
        Email BuildEmail(EmailContent emailContent);
    }
}
