﻿namespace OES.Core.Models
{
    public class EmailContent
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string AdminEmail { get; set; }
        public string LoginUrl { get; set; }
        public string Template { get; set; }
        public string Subject { get; set; }
        public string Telephone { get; set; }
        public string Password { get; set; }
    }
}
