﻿using Microsoft.AspNetCore.Authorization;

namespace OES.Core.Models
{
    public class AdminRequirement : IAuthorizationRequirement
    {
        public string IsAdmin { get; }

        public AdminRequirement(string isAdmin)
        {
            IsAdmin = isAdmin;
        }
    }
}
