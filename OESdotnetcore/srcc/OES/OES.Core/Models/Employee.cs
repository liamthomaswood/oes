﻿using OES.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OES.Core.Models
{
    public class Employee
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid EmployeeId { get; set; }

        [Required]
        [MaxLength(128)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(128)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(128)]
        public string EmailAddress { get; set; }

        [Required]
        public Department Department { get; set; }

        [MaxLength(128)]
        public string Telephone { get; set; }

        [Required]
        public Roles Role { get; set; }
        public bool IsSelected { get; set; }
    }
}
