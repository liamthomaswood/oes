﻿using OES.Core.Enumerations;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OES.Core.Models
{
    public class Question
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid QuestionId { get; set; }

        [Required(ErrorMessage = "Please enter a question")]
        [Column(Order = 2)]
        public string Text { get; set; }

        [Required]
        [Column(Order = 3)]
        public QuestionCategory ResponseCategory { get; set; }

        [Column(Order = 4)]
        public string AvailableChoices { get; set; }
    }
}
