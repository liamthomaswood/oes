﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace OES.Core.Models
{
    public class Email
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MailAddress From { get; set; }
    }
}
