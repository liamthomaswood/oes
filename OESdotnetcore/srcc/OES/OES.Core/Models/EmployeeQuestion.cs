﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OES.Core.Models
{
    public class EmployeeQuestion
    {
        [Key, Required, Column(Order = 1)]
        public Guid EmployeeQuestionId { get; set; }

        [Column(Order = 2)]
        public Guid QuestionId { get; set; }

        [Column(Order = 3)]
        public Guid EmployeeId { get; set; }

        public DateTime TimeAssigned { get; set; }
        public DateTime? ReminderSent { get; set; }
        public int NumberReminders { get; set; }
        public bool Answered { get; set; }
    }
}
