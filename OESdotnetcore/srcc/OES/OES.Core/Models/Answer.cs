﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace OES.Core.Models
{
    public class Answer
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid AnswerId { get; set; }

        [ForeignKey("Employee")]
        [Required]
        [Column(Order = 2)]
        public Guid EmployeeId { get; set; }

        [ForeignKey("Question")]
        [Required]
        [Column(Order = 3)]
        public Guid QuestionId { get; set; }

        [Column(Order = 4)]
        public string Value { get; set; }

        [Column(Order = 5)]
        public DateTime TimeResponded { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Question Question { get; set; }
    }
}
