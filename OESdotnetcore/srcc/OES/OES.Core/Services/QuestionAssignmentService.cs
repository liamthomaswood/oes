﻿using OES.Core.Gateways.Interfaces;
using OES.Core.Services.Interfaces;
using OES.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Linq;
using OES.Core.Handlers;

namespace OES.Core.Services
{
    public class QuestionAssignmentService : IQuestionAssignmentService
    {
        private readonly IRepository<EmployeeQuestion> _employeeQuestionRepo;
        private readonly IAccountEmailService _emailService;
        private readonly IEmployeeService _employeeService;

        public QuestionAssignmentService(IRepository<EmployeeQuestion> employeeQuestionRepo, IAccountEmailService emailService, IEmployeeService employeeService)
        {
            _employeeQuestionRepo = employeeQuestionRepo;
            _emailService = emailService;
            _employeeService = employeeService;
        }

        public string AssignQuestionToEmployee(EmployeeQuestion employeeQuestion, string requestUrl, bool sendEmail = true)
        {
            try
            {
                var employeeQuestionExists = _employeeQuestionRepo.GetAll().Any(eq => eq.EmployeeQuestionId == employeeQuestion.EmployeeQuestionId && !eq.Answered);
                if (employeeQuestionExists)
                {
                    return "Question has already been added";
                }
                
                var employee = _employeeService.FetchEmployees().Single(x => x.EmployeeId == employeeQuestion.EmployeeId);

                if (sendEmail)
                {
                    _emailService.SendNewQuestionEmail(employee.FirstName, employee.EmailAddress, requestUrl);
                }

                _employeeQuestionRepo.Create(employeeQuestion);

                return "Question has been assigned";
            }
            catch(Exception ex)
            {
                var message = ex.Message;
                return "Failed to assign question";
            }

        }

        public ServiceResponse AssignQuestionsToEmployeesMultiple(List<Guid> questionIdList, List<Guid> employeeIdList, string requestUrl, bool sendEmail = true)
        {
            var response = new ServiceResponse();
            var unassignedQuestions = 0;
            var assignedQuestions = 0;
            var responseMessage = new StringBuilder();

            try
            {
                if(questionIdList.Count == 0 || employeeIdList.Count == 0)
                {
                    response.ResponseMessage = "Either no question or employee has been specified";
                    return response;
                }
                foreach (var questionId in questionIdList)
                {
                    foreach (var employeeId in employeeIdList)
                    {
                        var hasAlreadyBeenAssigned = HasAlreadyBeenAssigned(employeeId, questionId);
                        if (!hasAlreadyBeenAssigned)
                        {
                            var employeeQuestion = new EmployeeQuestion()
                            {
                                EmployeeQuestionId = Guid.NewGuid(),
                                EmployeeId = employeeId,
                                QuestionId = questionId,
                                TimeAssigned = DateTime.UtcNow
                            };
                            AssignQuestionToEmployee(employeeQuestion, requestUrl, sendEmail);
                            assignedQuestions += 1;
                        }
                        else
                        {
                            unassignedQuestions += 1; 
                        }
                    }
                }

                string message;

                if(unassignedQuestions == 0 && assignedQuestions > 0)
                {
                    message = assignedQuestions > 1 ? "s have" : " has"; 
                    response.ResponseMessage = $"{assignedQuestions } Question{message} now been assigned and an email sent.";
                }
                else if(unassignedQuestions > 0 && assignedQuestions == 0)
                {
                    message = unassignedQuestions > 1 ? "s have" : " has";
                    response.ResponseMessage = $"{unassignedQuestions} Question{message} already been assigned and no action taken.";
                }
                else
                {
                    response.ResponseMessage =
                        $@"Questions Assigned: {assignedQuestions}
                           Questions Not Assigned: {unassignedQuestions}";
                }
                response.ItemsUpdated.Add(assignedQuestions);
                response.ItemsUpdated.Add(unassignedQuestions);

                return response;
            }
            catch
            {
                return new ServiceResponse
                {
                    ProcedureComplete = false,
                    ResponseMessage = "Sorry, question assignment failed."
                };
            }
        }

        public string UnassignQuestion(Guid employeeQuestionId)
        {
            try
            {
                var eq = _employeeQuestionRepo.GetAll().Single(x => x.EmployeeQuestionId == employeeQuestionId);
                _employeeQuestionRepo.Delete(eq.EmployeeQuestionId);
                return "Question Unassigned";
            }
            catch (Exception ex)
            {
                return "Failed to unassign question.";
            }
        }

        private bool HasAlreadyBeenAssigned(Guid employeeId, Guid questionId)
        {
            return _employeeQuestionRepo
                .GetAll()
                .Any(x => x.EmployeeId == employeeId && x.QuestionId == questionId && x.Answered == false);
        }
    }
}
