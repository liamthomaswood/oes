﻿using OES.Core.Enumerations;
using OES.Core.Gateways.Interfaces;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using OES.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OES.Core.Services
{
    public class EmployeeQuestionService : IEmployeeQuestionService
    {
        private readonly IRepository<EmployeeQuestion> _employeeQuestionRepo;
        private readonly IRepository<Employee> _employeeRepo;
        private readonly IRepository<Question> _questionRepo;
        private readonly IQuestionService _questionService;
        private readonly IEmployeeService _employeeService;

        public EmployeeQuestionService(
            IRepository<EmployeeQuestion> employeeQuestionRepo, 
            IRepository<Employee> employeeRepo,
            IRepository<Question> questionRepo,
            IQuestionService questionService,
            IEmployeeService employeeService)
        {
            _employeeQuestionRepo = employeeQuestionRepo;
            _employeeRepo = employeeRepo;
            _questionRepo = questionRepo;
            _questionService = questionService;
            _employeeService = employeeService;
        }

        public EmployeeQuestion GetEmployeeQuestionById(Guid employeeQuestionId)
        {
            return _employeeQuestionRepo.GetAll().FirstOrDefault(x => x.EmployeeQuestionId == employeeQuestionId);
        }

        public string SaveEmployeeQuestion(EmployeeQuestion employeeQuestionToUpdate)
        {
            try
            {
                _employeeQuestionRepo.Update(employeeQuestionToUpdate);
                return "Employee Question Saved";
            }
            catch
            {
                return "Failed to save employee question";
            }
        }

        public List<EmployeeQuestionViewModel> GetAssignedQuestionsToEmployees(Department filteredDepartment = 0, string search = null, QuestionCategory questionCategory = 0, string from = null, string to = null)
        {
            var eqvm = (from eq in _employeeQuestionRepo.GetAll()
                        join e in _employeeService.FetchEmployees(filteredDepartment, search) on eq.EmployeeId equals e.EmployeeId
                        join q in _questionService.GetQuestions(search) on eq.QuestionId equals q.QuestionId
                        where !eq.Answered
                        select new EmployeeQuestionViewModel
                        {
                            EmployeeQuestionId = eq.EmployeeQuestionId,
                            EmployeeName = e.EmailAddress.ToString(),
                            QuestionText = q.Text,
                            DateAssigned = eq.TimeAssigned.ToShortDateString(),
                            LastReminderDate = eq.ReminderSent == null? "" : eq.ReminderSent.GetValueOrDefault().ToShortDateString(),
                            RemindersSent = eq.NumberReminders.ToString()
                        });

            if (!string.IsNullOrEmpty(search))
            {
                eqvm = eqvm.Where(eq =>
                    eq.EmployeeName.Contains(search) ||
                    eq.QuestionText.Contains(search));
            }

            if (from != null && to != null)
            {
                DateTime.TryParse(from, out DateTime parsedFrom);
                DateTime.TryParse(to, out DateTime parsedTo);
                eqvm = eqvm.Where(
                    q => DateTime.Parse(q.DateAssigned) >= parsedFrom && DateTime.Parse(q.DateAssigned) <= parsedTo);
            }

            return eqvm.ToList();
        }
    }
}
