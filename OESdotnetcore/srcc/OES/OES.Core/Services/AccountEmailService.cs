﻿using OES.Core.Builders.Interfaces;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using System;

namespace OES.Core.Services
{
    public class AccountEmailService : IAccountEmailService
    {
        private readonly IEmailHandler _emailService;
        private readonly IEmailBuilder _emailBuilder;

        public AccountEmailService(IEmailHandler emailService, IEmailBuilder emailBuilder)
        {
            _emailService = emailService;
            _emailBuilder = emailBuilder;
        }

        public string SendNewQuestionEmail(string firstName, string to, string requestUrl)
        {
            try
            {
                var emailContent = new EmailContent
                {
                    Template = "QuestionAssigned",
                    FirstName = firstName,
                    Email = to,
                    Subject = "You have been assigned a new quetion",
                    LoginUrl = requestUrl
                };

                var email = _emailBuilder.BuildEmail(emailContent);
                _emailService.SendEmail(email);

                return "The email with the question has been sent successfully";
            }
            catch(Exception ex)
            {
                return "An error occurrsed when sending the email";
            }            
        }

        public string SendNewSignUpEmail(string firstName, string to, string password, string requestUrl)
        {
            try
            {
                var emailContent = new EmailContent
                {
                    Template = "NewSignupEmail",
                    FirstName = firstName,
                    Email = to,
                    Subject = "Welcome!",
                    LoginUrl = requestUrl,
                    Password = password
                };

                var email = _emailBuilder.BuildEmail(emailContent);
                _emailService.SendEmail(email);
                return "Success";
            }
            catch
            {
                return "Failed";
            }
        }

        public string SendPasswordResetEmail(string firstName, string to, string password, string requestUrl)
        {
            try
            {
                var emailContent = new EmailContent
                {
                    Template = "PasswordReset",
                    FirstName = firstName,
                    Email = to,
                    Subject = "Your OES Password has been reset",
                    LoginUrl = requestUrl,
                    Password = password
                };

                var email = _emailBuilder.BuildEmail(emailContent);
                _emailService.SendEmail(email);
                return "Success";
            }
            catch
            {
                return "Failed";
            }
        }
    }
}
