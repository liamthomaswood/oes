﻿using OES.Core.Gateways.Interfaces;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OES.Core.Services
{
    public class AnswerService : IAnswerService
    {
        private readonly IRepository<Answer> _answerRepository;
        private readonly IRepository<EmployeeQuestion> _employeeQuestionRepo;
        private readonly IEmailHandler _emailService;
        private readonly IEmployeeService _employeeService;

        public AnswerService(
            IRepository<Answer> answerRepo,
            IRepository<EmployeeQuestion> employeeQuestionRepo,
            IEmailHandler emailService,
            IEmployeeService employeeService)
        {
            _answerRepository = answerRepo;
            _employeeQuestionRepo = employeeQuestionRepo;
            _emailService = emailService;
            _employeeService = employeeService;
        }

        public bool AnswerQuestion(Answer answer, Guid userId)
        {
            //mark the eq object as answered
            var employeeQuestion = _employeeQuestionRepo.GetAll().FirstOrDefault(eq => eq.QuestionId == answer.QuestionId && eq.EmployeeId == userId && !eq.Answered);
            employeeQuestion.Answered = true;
            _employeeQuestionRepo.Update(employeeQuestion);

            //create the new answer
            answer.AnswerId = Guid.NewGuid();
            answer.TimeResponded = DateTime.UtcNow;
            answer.EmployeeId = employeeQuestion.EmployeeId;
            _answerRepository.Create(answer);            

            return true;
        }

        public List<Answer> GetAnswers()
        {
            var answers = _answerRepository.GetAll().ToList();
            return answers;
        }

        public List<Answer> GetAnswersForEmployee(Guid userId)
        {
            var answers = (from eq in _employeeQuestionRepo.GetAll()
                          where eq.EmployeeId == userId && eq.Answered == false
                          select new Answer
                          {
                              AnswerId = Guid.NewGuid(),
                              QuestionId = eq.QuestionId,
                              EmployeeId = userId                              
                          })
                          .ToList();

            return answers;
        }
    }
}
