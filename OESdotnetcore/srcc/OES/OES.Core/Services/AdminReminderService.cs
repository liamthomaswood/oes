﻿using OES.Core.Builders.Interfaces;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using System;
using System.Linq;

namespace OES.Core.Services
{
    public class AdminReminderService : IAdminReminderService
    {
        private readonly IEmailHandler _emailService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeQuestionService _employeeQuestionService;
        private readonly IEmailBuilder _emailBuilder;

        public AdminReminderService(
            IEmailHandler emailService, 
            IEmployeeService employeeService, 
            IEmployeeQuestionService employeeQuestionService,
            IEmailBuilder emailBuilder)
        {
            _emailService = emailService;
            _employeeService = employeeService;
            _employeeQuestionService = employeeQuestionService;
            _emailBuilder = emailBuilder;
        }

        public string SendReminderEmail(Guid employeeQuestionId, string requestUrl)
        {
            try
            {
                var eq = _employeeQuestionService.GetEmployeeQuestionById(employeeQuestionId);
                var employee = _employeeService.FetchEmployees().FirstOrDefault(x => x.EmployeeId == eq.EmployeeId);
                var emailContent = new EmailContent
                {
                    Template = "ReminderEmail",
                    FirstName = employee.FirstName,
                    Email = employee.EmailAddress,
                    Subject = "OES - Reminder",
                    LoginUrl = requestUrl
                };

                var email = _emailBuilder.BuildEmail(emailContent);
                _emailService.SendEmail(email);

                eq.NumberReminders += 1;
                eq.ReminderSent = DateTime.UtcNow;
                _employeeQuestionService.SaveEmployeeQuestion(eq);                

                return "Reminder email sent";
            }
            catch
            {
                return "Failed to send reminder email";
            }
        }       
    }
}
