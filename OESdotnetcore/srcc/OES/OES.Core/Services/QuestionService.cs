﻿using OES.Core.Gateways.Interfaces;
using OES.Core.Services.Interfaces;
using OES.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OES.Core.Enumerations;

namespace OES.Core.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IRepository<Question> _questionRepository;
        private readonly IRepository<EmployeeQuestion> _employeeQuestionRepository;

        public QuestionService(
            IRepository<Question> questionRepository, IRepository<EmployeeQuestion> employeeQuestionRepository)
        {
            _questionRepository = questionRepository;
            _employeeQuestionRepository = employeeQuestionRepository;
        }

        public string AddQuestion(Question question)
        {
            var questionExists = _questionRepository.GetAll().Any(x => x.QuestionId == question.QuestionId || x.Text == question.Text);
            if (questionExists)
            {
                return "The question already exists, please double check and try again.";
            }

            _questionRepository.Create(question);
            return "Question added successfully";
        }

        public IEnumerable<Question> GetQuestions(string search = null, string questionFormat = null)
        {
            IEnumerable<Question> questions;
            if (string.IsNullOrEmpty(search))
            {
                questions = _questionRepository.GetAll();
            }
            else
            {
                questions = _questionRepository.GetAll().Where(
                    eq => eq.Text.Contains(search));
            }
            if (!string.IsNullOrEmpty(questionFormat))
            {
                Enum.TryParse(questionFormat, true, out QuestionCategory questionCategory);
                questions = questions.Where(eq => eq.ResponseCategory == questionCategory);
            }
            return questions.AsEnumerable();
        }

        public IEnumerable<Question> GetQuestionsForEmployee(Guid id)
        {
            var questions = (from eq in _employeeQuestionRepository.GetAll()
                             join q in _questionRepository.GetAll() on eq.QuestionId equals q.QuestionId
                             where eq.EmployeeId == id
                             select q)
                             .AsEnumerable();

            return questions;
        }

        public IEnumerable<Question> GetUnansweredQuestionsForEmployee(Guid id)
        {
            var questions = (from eq in _employeeQuestionRepository.GetAll()
                            join q in _questionRepository.GetAll() on eq.QuestionId equals q.QuestionId
                            where eq.EmployeeId == id 
                            && eq.Answered != true
                            select q)
                           .AsEnumerable();

            return questions;
        }


        public IEnumerable<Question> GetAnsweredQuestionsForEmployee(Guid id)
        {
            var questions = (from eq in _employeeQuestionRepository.GetAll()
                             join q in _questionRepository.GetAll() on eq.QuestionId equals q.QuestionId
                             where eq.EmployeeId == id
                             && eq.Answered == true
                             select q)
                           .AsEnumerable();

            return questions;
        }

        public string EditQuestion(Guid questionId, string questionText, string category, string availableChoices)
        {
            try
            {
                var question = _questionRepository.GetAll().Single(q => q.QuestionId == questionId);
                question.Text = questionText;
                question.ResponseCategory = (QuestionCategory)Enum.Parse(typeof(QuestionCategory), category);
                question.AvailableChoices = availableChoices;
                _questionRepository.Update(question);
                return "Question Edited Successfully";
            }
            catch (Exception ex)
            {
                return "Failed to edit question";
            }

        }

        public string DeleteQuestion(Guid questionId)
        {
            try
            {
                _questionRepository.Delete(questionId);
                return "Question Deleted successfully";
            }
            catch (Exception ex)
            {
                return "Failed to delete question";
            }
         }
    }
}
