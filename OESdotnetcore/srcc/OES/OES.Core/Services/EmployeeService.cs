﻿using OES.Core.Enumerations;
using OES.Core.Gateways.Interfaces;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OES.Core.Services
{
    public class EmployeeService : IEmployeeService
    {
        public IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepo)
        {
            _employeeRepository = employeeRepo;
        }

        public List<Employee> FetchEmployees(Department department, string search = null)
        {
            var employees = _employeeRepository.GetAll();

            if(!string.IsNullOrEmpty(search))
            {
                employees = employees.Where(e =>
                    e.LastName.Contains(search) ||
                    e.FirstName.Contains(search) ||
                    e.EmailAddress.Contains(search));
            }

            if (department != 0)
            {
                employees = employees.Where(e => e.Department == department);
            }
            return employees.ToList();
        }

        public Employee GetEmployeeByEmail(string email)
        {
            if (email == null)
            {
                return null;
            }

            var employee = _employeeRepository.GetAll().Single(em => em.EmailAddress == email);
            return employee;
        }

        public string CreateNewEmployee(Employee employee)
        {
            var employeeExists = _employeeRepository.GetAll().Any(x => x.EmailAddress == employee.EmailAddress);
            if (employeeExists)
            {
                return "person already exists";
            }
            _employeeRepository.Create(employee);
            return "employee created successfully";
        }

        public string EditEmpoyee(Employee employee)
        {
            var employeeExists = _employeeRepository.GetAll().Any(x => x.EmployeeId == employee.EmployeeId);
            if (employeeExists)
            {
                var employeeToUpdate = _employeeRepository.GetAll().Single(e => e.EmployeeId == employee.EmployeeId);
                employeeToUpdate = employee;
                _employeeRepository.Update(employeeToUpdate);
                return "Employee Updated Successfully";
            }
            return "Employee already exists.";
        }


        public string DeleteEmployee(Guid employeeId)
        {
            var employeeExists = _employeeRepository.GetAll().Any(e => e.EmployeeId == employeeId);
            if (employeeExists)
            {
                _employeeRepository.Delete(_employeeRepository.GetAll().Single(e => e.EmployeeId == employeeId).EmployeeId);
                return "Employee deleted successfully";
            }
            return "The employee was not found.";
        }

        public string ResetPassword(Guid employeeId)
        {
            return "password reset successfully";
        }
    }
}
