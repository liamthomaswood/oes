﻿using OES.Core.Handlers;
using OES.Core.Models;
using System;
using System.Collections.Generic;

namespace OES.Core.Services.Interfaces
{
    public interface IQuestionAssignmentService
    {
        string AssignQuestionToEmployee(EmployeeQuestion employeeQuestion, string requestUrl, bool sendEmail = true);
        ServiceResponse AssignQuestionsToEmployeesMultiple(List<Guid> questionIdList, List<Guid> employeeIdList, string requestUrl, bool sendEmail = true);
        string UnassignQuestion(Guid employeeQuestionId);
    }
}
