﻿using OES.Core.ViewModels;
using System;
using System.Collections.Generic;

namespace OES.Core.Services.Interfaces
{
    public interface IEmployeeQuestionAnswerService
    {
        List<EmployeeQuestionAnswer> GetEmployeeQuestionAnswers(string search = null, string from = null, string to = null);
        EmployeeUnansweredViewModel GetEmployeeQuestionViewModel(Guid employeeId);
        string DeleteEmployeeData(string employeeId);
    }
}
