﻿using OES.Core.Models;
using System;
using System.Collections.Generic;

namespace OES.Core.Services.Interfaces
{
    public interface IQuestionService
    {
        string AddQuestion(Question question);
        string EditQuestion(Guid questionId, string questionText, string category, string availableChoices);
        string DeleteQuestion(Guid questionId);
        IEnumerable<Question> GetQuestions(string search = null, string questionFormat = null);
        IEnumerable<Question> GetQuestionsForEmployee(Guid id);
        IEnumerable<Question> GetUnansweredQuestionsForEmployee(Guid id);
        IEnumerable<Question> GetAnsweredQuestionsForEmployee(Guid id);
    }
}
