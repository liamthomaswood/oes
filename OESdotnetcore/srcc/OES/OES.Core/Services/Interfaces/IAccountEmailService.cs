﻿namespace OES.Core.Services.Interfaces
{
    public interface IAccountEmailService
    {
        string SendNewQuestionEmail(string firstName, string to, string requestUrl);
        string SendNewSignUpEmail(string firstName, string to, string password, string requestUrl);
        string SendPasswordResetEmail(string firstName, string to, string password, string requestUrl);
    }
}
