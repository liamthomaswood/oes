﻿using System;

namespace OES.Core.Services.Interfaces
{
    public interface IAdminReminderService
    {
        string SendReminderEmail(Guid employeeQuestionId, string requestUrl);
    }
}
