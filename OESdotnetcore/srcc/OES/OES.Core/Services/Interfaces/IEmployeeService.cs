﻿using OES.Core.Enumerations;
using OES.Core.Models;
using System;
using System.Collections.Generic;

namespace OES.Core.Services.Interfaces
{
    public interface IEmployeeService
    {
        Employee GetEmployeeByEmail(string email);
        List<Employee> FetchEmployees(Department department = 0, string search = null);
        string CreateNewEmployee(Employee employee);
        string EditEmpoyee(Employee employee);
        string DeleteEmployee(Guid employeeId);
        string ResetPassword(Guid employeeId);
    }
}
