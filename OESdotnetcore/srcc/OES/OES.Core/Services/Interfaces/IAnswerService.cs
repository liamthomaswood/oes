﻿using OES.Core.Models;
using OES.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace OES.Core.Services.Interfaces
{
    public interface IAnswerService
    {
        List<Answer> GetAnswers();
        List<Answer> GetAnswersForEmployee(Guid userId);
        bool AnswerQuestion(Answer answer, Guid userId);
    }
}
