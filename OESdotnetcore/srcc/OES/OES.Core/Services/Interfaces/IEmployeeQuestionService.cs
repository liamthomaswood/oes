﻿using OES.Core.Enumerations;
using OES.Core.Models;
using OES.Core.ViewModels;
using System;
using System.Collections.Generic;

namespace OES.Core.Services.Interfaces
{
    public interface IEmployeeQuestionService
    {
        EmployeeQuestion GetEmployeeQuestionById(Guid employeeQuestionId);
        string SaveEmployeeQuestion(EmployeeQuestion employeeQuestion);
        List<EmployeeQuestionViewModel> GetAssignedQuestionsToEmployees(Department filteredDepartment = 0, string search = null, QuestionCategory filteredCategory = 0, string from = null, string to = null);
    }
}
