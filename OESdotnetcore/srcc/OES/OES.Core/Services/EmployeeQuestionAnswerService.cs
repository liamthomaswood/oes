﻿using Microsoft.AspNetCore.Identity;
using OES.Core.Enumerations;
using OES.Core.Gateways.Interfaces;
using OES.Core.Models;
using OES.Core.Services.Interfaces;
using OES.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OES.Core.Services
{
    public class EmployeeQuestionAnswerService : IEmployeeQuestionAnswerService
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<EmployeeQuestion> _employeeQuestionRepository;
        private readonly IRepository<Question> _questionRepostiory;
        private readonly IRepository<Answer> _answerRepository;
        private readonly IQuestionService _questionService;

        public EmployeeQuestionAnswerService(
            IRepository<Employee> employeeRepo,
            IRepository<EmployeeQuestion> employeeQuestionRepo,
            IRepository<Question> questionRepo,
            IRepository<Answer> answerRepo,
            IQuestionService questionService)
        {
            _employeeRepository = employeeRepo;
            _employeeQuestionRepository = employeeQuestionRepo;
            _questionRepostiory = questionRepo;
            _answerRepository = answerRepo;
            _questionService = questionService;
        }

        public string DeleteEmployeeData(string employeeId)
        {
            var guidId = Guid.Parse(employeeId);
            var employee = _employeeRepository.GetAll().FirstOrDefault(e => e.EmployeeId == guidId);
            var questions = _employeeQuestionRepository.GetAll().Where(q => q.EmployeeId == guidId);
            var answers = _answerRepository.GetAll().Where(a => a.EmployeeId == guidId);
            try
            {
                _employeeRepository.Delete(employee.EmployeeId);
                questions.ToList().ForEach(q => _questionRepostiory.Delete(q.EmployeeId));
                answers.ToList().ForEach(a => _answerRepository.Delete(a.EmployeeId));

                return "Employee Data deleted successfully";
            }
            catch(Exception ex)
            {
                return string.Format("An error occurred when deleting the employee data /n {0}", ex.Message);
            }
        }

        public List<EmployeeQuestionAnswer> GetEmployeeQuestionAnswers(string search = null, string from = null, string to = null)
        {
            var employeeQuestionAnswers = (from a in _answerRepository.GetAll()
                                           join e in _employeeRepository.GetAll() on a.EmployeeId equals e.EmployeeId
                                           join q in _questionRepostiory.GetAll() on a.QuestionId equals q.QuestionId
                                           select new EmployeeQuestionAnswer
                                           {
                                               Answer = a,
                                               Question = q,
                                               Employee = e
                                           });

            if(search != null)
            {
                employeeQuestionAnswers = employeeQuestionAnswers.Where(
                    q => q.Answer.Value.Contains(search) ||
                    q.Question.Text.Contains(search) ||
                    q.Employee.EmailAddress.Contains(search));
            }

            if(from != null && to != null)
            {
                DateTime.TryParse(from, out DateTime parsedFrom);
                DateTime.TryParse(to, out DateTime parsedTo);
                employeeQuestionAnswers = employeeQuestionAnswers.Where(
                    q => q.Answer.TimeResponded >= parsedFrom && q.Answer.TimeResponded <= parsedTo);
            }

            return employeeQuestionAnswers.ToList();
        }

        public EmployeeUnansweredViewModel GetEmployeeQuestionViewModel(Guid employeeId)
        {
            var employeeQuestions = _questionService.GetUnansweredQuestionsForEmployee(employeeId).ToList();
            if(employeeQuestions.Count > 0)
            {
                var question = employeeQuestions.FirstOrDefault();

                var model = new EmployeeUnansweredViewModel
                {
                    QuestionToAnswer = question,
                    QuestionCount = employeeQuestions.Count,
                };

                if (question.ResponseCategory != QuestionCategory.FreeText)
                {
                    model.AvailableChoices = question.AvailableChoices.Split(',').ToList();
                }
                return model;
            }
            return new EmployeeUnansweredViewModel();
        }
    }
}
