﻿using Microsoft.AspNetCore.Mvc.Rendering;
using OES.Core.Enumerations;
using System;
using System.Collections.Generic;

namespace OES.Core.ViewModels
{
    public class AssignQuestionViewModel
    {
        public List<EmployeeQuestionViewModel> EmployeeQuestionViewModel { get; set; }
        public SelectList EmployeeSelectList { get; set; }
        public SelectList QuestionsSelectList { get; set; }
        public List<Guid> QuestionIds { get; set; }
        public List<Guid> EmployeeIds { get; set; }
        public string Search { get; set; }
        public Department FilteredDepartment { get; set; }
        public QuestionCategory CategoryFilter { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
