﻿using OES.Core.Enumerations;

namespace OES.Core.ViewModels
{
    public class EmployeeEditModel
    {
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string LastEmail { get; set; }
        public Department Department { get; set; }
        public Roles Role { get; set; }
    }
}
