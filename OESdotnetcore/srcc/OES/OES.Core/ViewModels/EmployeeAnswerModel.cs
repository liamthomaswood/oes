﻿using System.Collections.Generic;

namespace OES.Core.ViewModels
{
    public class EmployeeAnswerModel
    {
        public string Search { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public List<EmployeeQuestionAnswer> EmployeeQuestionAnswerModel { get; set; }
    }
}
