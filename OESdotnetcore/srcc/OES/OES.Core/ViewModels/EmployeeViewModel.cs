﻿using OES.Core.Enumerations;
using OES.Core.Models;
using System.Collections.Generic;

namespace OES.Core.ViewModels
{
    public class EmployeeViewModel
    {
        public Employee Employee { get; set; }
        public List<Employee> EmployeeList { get; set; }
        public string Search { get; set; }
        public Department FilteredDepartment { get; set; }
    }
}
