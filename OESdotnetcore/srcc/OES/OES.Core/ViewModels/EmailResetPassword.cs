﻿using System.ComponentModel.DataAnnotations;

namespace OES.Core.ViewModels
{
    public class EmailResetPassword
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Your Email")]
        public string EmployeeEmail { get; set; }
    }
}
