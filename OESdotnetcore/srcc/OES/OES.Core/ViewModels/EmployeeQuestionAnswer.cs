﻿using OES.Core.Models;

namespace OES.Core.ViewModels
{
    public class EmployeeQuestionAnswer
    {
        public Employee Employee { get; set; }
        public Question Question { get; set; }
        public Answer Answer { get; set; }
    }
}
