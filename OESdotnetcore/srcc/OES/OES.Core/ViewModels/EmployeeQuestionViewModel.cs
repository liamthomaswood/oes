﻿using System;

namespace OES.Core.ViewModels
{
    public class EmployeeQuestionViewModel
    {
        public Guid EmployeeQuestionId { get; set; }
        public string EmployeeName { get; set; }
        public string QuestionText { get; set; }
        public string DateAssigned { get; set; }
        public bool SendReminder { get; set; }
        public string RemindersSent { get; set; }
        public string LastReminderDate { get; set; }
    }
}
