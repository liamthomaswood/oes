﻿using OES.Core.Models;
using System.Collections.Generic;

namespace OES.Core.ViewModels
{
    public class EmployeeUnansweredViewModel
    {
        public Answer Answer { get; set; }
        public List<string> AvailableChoices { get; set; }
        public Question QuestionToAnswer { get; set; }
        public int QuestionCount { get; set; }
    }
}
