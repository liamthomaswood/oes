﻿using OES.Core.Enumerations;
using OES.Core.Models;
using System.Collections.Generic;

namespace OES.Core.ViewModels
{
    public class AdminQuestionViewModel
    {
        public Question Question { get; set; }
        public List<Question> QuestionList { get; set; }
        public QuestionCategory ChosenCategory { get; set; }
        public List<QuestionCategory> QuestionCategories { get; set; }
        public string Search { get; set; }        
        public string QuestionFormat { get; set; }
    }
}
