﻿using System.ComponentModel;

namespace OES.Core.Enumerations
{
    public enum Department
    {
        [Description("Technology")]
        Technology = 1,
        [Description("Sales")]
        Sales = 2,
        [Description("Corporate")]
        Corporate = 3,
        [Description("Account Management")]
        AccountManagement = 4
    }
}
