﻿using System.ComponentModel;

namespace OES.Core.Enumerations
{
    public enum QuestionCategory
    {
        [Description("radio")]
        Radio = 1,
        [Description("checkbox")]
        CheckBox = 2,
        [Description("freetext")]
        FreeText = 3,
    }
}
