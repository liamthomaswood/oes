﻿using OES.Core.Models;

namespace OES.Core.Handlers.Interfaces
{
    public interface IEmailHandler
    {
        string SendEmail(Email email);
    }
}
