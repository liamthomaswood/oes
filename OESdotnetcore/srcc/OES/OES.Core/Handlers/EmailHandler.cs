﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using OES.Core.Configuration;
using OES.Core.Handlers.Interfaces;
using OES.Core.Models;
using System;
using System.Net.Mail;

namespace OES.Core.Handlers
{
    public class EmailHandler : IEmailHandler
    {
        private readonly Config _config;
        private readonly IHostingEnvironment _env;

        public EmailHandler(IOptions<Config> config, IHostingEnvironment env)
        {
            _config = config.Value;
            _env = env;
        }

        public string SendEmail(Email email)
        {
            try
            {
                using (var smtp = new SmtpClient())
                {
                    if (_env.IsDevelopment())
                    {
                        smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                        smtp.PickupDirectoryLocation = @"c:\smtp";

                    }
                    else
                    {
                        smtp.Host = _config.SmtpHost;
                        smtp.Credentials = new System.Net.NetworkCredential(_config.SmtpUser, _config.SmtpPwd);
                        
                    }
                    var message = new MailMessage()
                    {
                        Subject = email.Subject,
                        Body = email.Body,
                        IsBodyHtml = true,
                        From = new MailAddress(_config.EmailFrom)
                    };
                    message.To.Add(email.To);

                    smtp.Send(message);
                }
                return "Email sent successfully";
            }
            catch (Exception ex)
            {
                var exception = ex.Message;
                return "Sorry, an error occurred";
            }
        }
    }
}
