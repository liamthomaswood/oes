﻿using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace OES.SurveyApp.Handlers
{
    public class AdminRequirementHandler : AuthorizationHandler<Core.Models.AdminRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, Core.Models.AdminRequirement requirement)
        {
            if(context.User.HasClaim(c => c.Type == "Admin"))
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
