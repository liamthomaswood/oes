﻿using System.Collections.Generic;

namespace OES.Core.Handlers
{
    public class ServiceResponse
    {
        public bool ProcedureComplete { get; set; }
        public string ResponseMessage { get; set; }
        public List<int> ItemsUpdated { get; set; }

        public ServiceResponse()
        {
            this.ItemsUpdated = new List<int>();
            this.ProcedureComplete = true;
            this.ResponseMessage = "Procedure Complete";
        }
    }
}
