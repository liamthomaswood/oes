﻿using OES.Entities.Answers;
using OES.Entities.Questions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OES.Entities.QuestionAnswers
{
    public class QuestionAnswer
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid Id {get;set;}

        [Key]
        [ForeignKey("Question")]
        [Required]
        [Column(Order = 2)]
        public Guid QuestionId { get; set; }

        [Key]
        [ForeignKey("Answer")]
        [Required]
        [Column(Order = 3)]
        public Guid AnswerId { get; set; }

        public virtual ICollection<Question> Question { get; set; }

        public virtual ICollection<Answer> Answer { get; set; }
    }
}
