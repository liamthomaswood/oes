﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OES.Entities.Employee.ViewModels
{
    public class EmployeeDisplayViewModel
    {
        [Display(Name = "Employee Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }
    }
}
