﻿using OES.Entities.Emails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OES.Entities.EmailTrackers
{
    public class EmailTracker
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid EmailTrackerId { get; set; }

        [Key]
        [ForeignKey("Email")]
        [Required]
        [Column(Order = 2)]
        public Guid EmailId { get; set; }

        [Column(Order = 3)]
        [Required]
        public DateTime TimeSent { get; set; }

        [Column(Order = 4)]
        [Required]
        public DateTime TimeReplied { get; set; }

        [Column(Order = 5)]
        [Required]
        public bool ReminderSent { get; set; }

        public virtual ICollection<Email> Email { get; set; }
    }
}
