﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OES.Entities.Answers;
using OES.Entities.QuestionAnswers;

namespace OES.Entities.Questions
{
    public enum QuestionCategory
    {
        Likert = 1,
        FreeText = 2
    }
    public class Question
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid QuestionId { get; set; }

        [Required]
        [Column(Order = 2)]
        public string Text { get; set; }
        
        [Required]
        [Column(Order = 3)]
        public QuestionCategory QuestionCategory { get; set; }
        
        public virtual ICollection<QuestionAnswer> QuestionAnswer { get; set; }
    }
}
