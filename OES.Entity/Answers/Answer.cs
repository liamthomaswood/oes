﻿using OES.Entities.Employees;
using OES.Entities.Questions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OES.Entities.Answers
{
    public class Answer
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid AnswerId { get; set; }

        [Key]
        [ForeignKey("Employee")]
        [Required]
        [Column(Order = 2)]
        public Guid EmployeeId { get; set; }

        [Column(Order = 3)]
        public string Value { get; set; }

        public virtual Employees.Employee Employee { get; set; }
    }
}
