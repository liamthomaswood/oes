﻿using OES.Entities.Emails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OES.Entities.Employees
{
    public enum Roles
    {
        Developer = 1,
        Manager = 2
    }
    public class Employee
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid EmployeeId { get; set; }

        [Required]
        [MaxLength(128)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(128)]
        public string LastName { get; set; }

        [Required]
        public Roles Role { get; set; }

        public virtual ICollection<Email> Email { get; set; }
    }
}