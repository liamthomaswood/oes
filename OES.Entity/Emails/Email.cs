﻿using OES.Entities.EmailTrackers;
using OES.Entities.Employees;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OES.Entities.Emails
{
    public class Email
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public Guid EmailId { get; set; }

        [Key]
        [Required]
        [ForeignKey("Employee")]
        [Column(Order = 2)]
        public Guid EmployeeId { get; set; }

        [Column(Order = 3)]
        public string Questions { get; set; }
        
        public virtual Employees.Employee Employee { get; set; }
    }
}
